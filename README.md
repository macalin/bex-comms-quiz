Requirements:
- Java 8
- Tomcat 7
- MySQL 5.7
- node js

Build:
- copy mysql connector jar in Tomcat lib dir: can be found on mySQL page: https://dev.mysql.com/downloads/connector/j/5.0.html
- 'npm install' in project root
- 'npm install' in src/main/ui
- 'mvn clean install' in project root
- for prod: 'mvn clean install -P prod'
- in SQL:
    - run create-schema.sql
    - run install.sql
    - edit and run populate-admins.sql


TODO: bugs & features
- bug: logout non functional on safari?
- answer max length > 25
- logging don't work?

TODO: Nice to have:
- make gulp uglify work
- secure email sender
- encrypt email password in properties file
- user text answer length server validation
- after countdown finish in active tab -> redirect to completed

Technical:
- reduce redundancy: make a service for : computeSingleChoiceModel function
- remove now() from sql scripts for older mysql versions