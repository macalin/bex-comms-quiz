insert into tbl_user (username, first_name, last_name, user_role_id, email, date_created)
values
  ('alexandru.cergau', 'Alexandru', 'Cergau', 2 ,'alexandru.cergau@db.com', sysdate());
insert into tbl_user (username, first_name, last_name, user_role_id, email, date_created)
values
  ('mihail.calin', 'Mihail', 'Calin', 2 ,'mihail.calin@db.com', sysdate());
insert into tbl_user (username, first_name, last_name, user_role_id, email, date_created)
values
  ('rada.ciurea', 'Rada', 'Ciurea', 2 ,'rada.ciurea@db.com', sysdate());
insert into tbl_user (username, first_name, last_name, user_role_id, email, date_created)
values
  ('mirela-b.lupu', 'Mirela B', 'Lupu', 2 ,'mirela-b.lupu@db.com', sysdate());
COMMIT;
