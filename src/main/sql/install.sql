
CREATE TABLE tbl_user_role (
                user_role_id INT AUTO_INCREMENT NOT NULL,
                code VARCHAR(10) NOT NULL,
                label VARCHAR(100) NOT NULL,
                PRIMARY KEY (user_role_id)
);


CREATE TABLE tbl_question_type (
                question_type_id INT AUTO_INCREMENT NOT NULL,
                code VARCHAR(8) NOT NULL,
                label VARCHAR(50) NOT NULL,
                PRIMARY KEY (question_type_id)
);

ALTER TABLE tbl_question_type COMMENT 'The table that stores the question types (QSTT)';

ALTER TABLE tbl_question_type MODIFY COLUMN code VARCHAR(8) COMMENT 'Question type code';

ALTER TABLE tbl_question_type MODIFY COLUMN label VARCHAR(50) COMMENT 'Question type label';


CREATE TABLE tbl_quiz_status (
                quiz_status_id INT AUTO_INCREMENT NOT NULL,
                code VARCHAR(10) NOT NULL,
                label VARCHAR(20) NOT NULL,
                PRIMARY KEY (quiz_status_id)
);


CREATE TABLE tbl_user (
                username VARCHAR(64) NOT NULL,
                first_name VARCHAR(50) NOT NULL,
                last_name VARCHAR(50) NOT NULL,
                user_role_id INT NOT NULL,
                email VARCHAR(60) NOT NULL,
                date_created DATETIME DEFAULT Now() NOT NULL,
                date_updated DATETIME,
                PRIMARY KEY (username)
);

ALTER TABLE tbl_user COMMENT 'Table that stores the application''s users';


CREATE TABLE tbl_quiz (
                quiz_id INT AUTO_INCREMENT NOT NULL,
                quiz_status_id INT NOT NULL,
                name VARCHAR(100) NOT NULL,
                duration NUMERIC(3) NOT NULL,
                date_started DATETIME,
                date_created DATETIME DEFAULT NOW() NOT NULL,
                user_created VARCHAR(64) NOT NULL,
                date_updated DATETIME,
                user_updated VARCHAR(64),
                PRIMARY KEY (quiz_id)
);

ALTER TABLE tbl_quiz COMMENT 'The table that stores the quizes (QUIZ)';

ALTER TABLE tbl_quiz MODIFY COLUMN quiz_status_id INTEGER COMMENT 'Quiz status foreign key';


CREATE TABLE tbl_user_quiz (
                user_quiz_id INT AUTO_INCREMENT NOT NULL,
                quiz_id INT NOT NULL,
                user_id VARCHAR(64) NOT NULL,
                score SMALLINT,
                date_created DATETIME DEFAULT NOW() NOT NULL,
                date_updated DATETIME,
                PRIMARY KEY (user_quiz_id)
);

ALTER TABLE tbl_user_quiz MODIFY COLUMN score SMALLINT COMMENT 'User score for a quiz';


CREATE TABLE tbl_question (
                question_id INT AUTO_INCREMENT NOT NULL,
                quiz_id INT NOT NULL,
                question_type_id INT NOT NULL,
                text VARCHAR(250) NOT NULL,
                date_created DATETIME DEFAULT NOW() NOT NULL,
                user_created VARCHAR(64) NOT NULL,
                date_updated DATETIME,
                user_updated VARCHAR(64),
                PRIMARY KEY (question_id)
);

ALTER TABLE tbl_question COMMENT 'The table that stores the questions (QSTN)';

ALTER TABLE tbl_question MODIFY COLUMN quiz_id INTEGER COMMENT 'Quiz foreign key';

ALTER TABLE tbl_question MODIFY COLUMN question_type_id INTEGER COMMENT 'Question type foreign key';


CREATE TABLE tbl_question_statistic (
                question_id INT NOT NULL,
                correct_answers_pct DECIMAL(5,4) NOT NULL,
                date_created DATETIME DEFAULT Now() NOT NULL,
                date_updated DATETIME,
                PRIMARY KEY (question_id)
);


CREATE TABLE tbl_answer (
                answer_id INT AUTO_INCREMENT NOT NULL,
                question_id INT NOT NULL,
                text VARCHAR(300) NOT NULL,
                date_created DATETIME DEFAULT NOW() NOT NULL,
                user_created VARCHAR(64) NOT NULL,
                date_updated DATETIME,
                user_updated VARCHAR(64),
                PRIMARY KEY (answer_id)
);

ALTER TABLE tbl_answer COMMENT 'The table that stores the question''s answers (ANSR)';

ALTER TABLE tbl_answer MODIFY COLUMN question_id INTEGER COMMENT 'Question foreign key';

ALTER TABLE tbl_answer MODIFY COLUMN text VARCHAR(300) COMMENT 'Answer text';

ALTER TABLE tbl_answer MODIFY COLUMN user_created VARCHAR(64) COMMENT 'User that created this answer. User foreign key.';

ALTER TABLE tbl_answer MODIFY COLUMN user_updated VARCHAR(64) COMMENT 'User that updated this answer. User foreign key.';


CREATE TABLE tbl_correct_answer (
                correct_answer_id INT AUTO_INCREMENT NOT NULL,
                question_id INT NOT NULL,
                answer_id INT,
                text VARCHAR(100),
                date_created DATETIME DEFAULT NOW() NOT NULL,
                user_created VARCHAR(64) NOT NULL,
                date_updated DATETIME,
                user_updated VARCHAR(64),
                PRIMARY KEY (correct_answer_id)
);

ALTER TABLE tbl_correct_answer MODIFY COLUMN answer_id INTEGER COMMENT 'Answer ID';

ALTER TABLE tbl_correct_answer MODIFY COLUMN text VARCHAR(100) COMMENT 'For text question types';


CREATE TABLE tbl_user_answer (
                user_answer_id INT AUTO_INCREMENT NOT NULL,
                user_quiz_id INT NOT NULL,
                question_id INT NOT NULL,
                answer_id INT,
                text VARCHAR(100),
                date_created DATETIME DEFAULT NOW() NOT NULL,
                date_updated DATETIME,
                PRIMARY KEY (user_answer_id)
);

ALTER TABLE tbl_user_answer MODIFY COLUMN answer_id INTEGER COMMENT 'Answer ID';


ALTER TABLE tbl_user ADD CONSTRAINT tbl_user_role_tbl_user_fk
FOREIGN KEY (user_role_id)
REFERENCES tbl_user_role (user_role_id)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE tbl_question ADD CONSTRAINT question_question_type_fk
FOREIGN KEY (question_type_id)
REFERENCES tbl_question_type (question_type_id)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE tbl_quiz ADD CONSTRAINT tbl_quiz_status_tbl_quiz_fk
FOREIGN KEY (quiz_status_id)
REFERENCES tbl_quiz_status (quiz_status_id)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE tbl_quiz ADD CONSTRAINT quiz_user_fk
FOREIGN KEY (user_created)
REFERENCES tbl_user (username)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE tbl_answer ADD CONSTRAINT tbl_user_tbl_answer_fk
FOREIGN KEY (user_created)
REFERENCES tbl_user (username)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE tbl_question ADD CONSTRAINT tbl_user_tbl_question_fk
FOREIGN KEY (user_created)
REFERENCES tbl_user (username)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE tbl_quiz ADD CONSTRAINT tbl_user_tbl_quiz_fk
FOREIGN KEY (user_updated)
REFERENCES tbl_user (username)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE tbl_question ADD CONSTRAINT tbl_user_tbl_question_fk1
FOREIGN KEY (user_updated)
REFERENCES tbl_user (username)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE tbl_answer ADD CONSTRAINT tbl_user_tbl_answer_fk1
FOREIGN KEY (user_updated)
REFERENCES tbl_user (username)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE tbl_user_quiz ADD CONSTRAINT tbl_user_tbl_user_answer_fk
FOREIGN KEY (user_id)
REFERENCES tbl_user (username)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE tbl_correct_answer ADD CONSTRAINT tbl_user_tbl_correct_answer_fk
FOREIGN KEY (user_created)
REFERENCES tbl_user (username)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE tbl_correct_answer ADD CONSTRAINT tbl_user_tbl_correct_answer_fk1
FOREIGN KEY (user_updated)
REFERENCES tbl_user (username)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE tbl_question ADD CONSTRAINT tbl_quiz_tbl_question_fk
FOREIGN KEY (quiz_id)
REFERENCES tbl_quiz (quiz_id)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE tbl_user_quiz ADD CONSTRAINT tbl_quiz_tbl_user_answer_fk
FOREIGN KEY (quiz_id)
REFERENCES tbl_quiz (quiz_id)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE tbl_user_answer ADD CONSTRAINT tbl_user_question_tbl_user_answer_fk
FOREIGN KEY (user_quiz_id)
REFERENCES tbl_user_quiz (user_quiz_id)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE tbl_answer ADD CONSTRAINT tbl_question_tbl_answer_fk
FOREIGN KEY (question_id)
REFERENCES tbl_question (question_id)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE tbl_correct_answer ADD CONSTRAINT tbl_question_tbl_correct_answer_fk
FOREIGN KEY (question_id)
REFERENCES tbl_question (question_id)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE tbl_user_answer ADD CONSTRAINT tbl_question_tbl_user_answer_fk
FOREIGN KEY (question_id)
REFERENCES tbl_question (question_id)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE tbl_question_statistic ADD CONSTRAINT tbl_question_tbl_quiz_statistic_fk
FOREIGN KEY (question_id)
REFERENCES tbl_question (question_id)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE tbl_user_answer ADD CONSTRAINT tbl_answer_tbl_user_answer_fk
FOREIGN KEY (answer_id)
REFERENCES tbl_answer (answer_id)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE tbl_correct_answer ADD CONSTRAINT tbl_answer_tbl_correct_answer_fk
FOREIGN KEY (answer_id)
REFERENCES tbl_answer (answer_id)
ON DELETE NO ACTION
ON UPDATE NO ACTION;


ALTER TABLE tbl_question_type AUTO_INCREMENT = 1;
ALTER TABLE tbl_quiz_status AUTO_INCREMENT = 1;

insert into tbl_question_type(code, label)
values
  ('SINGLE', 'Single answer'),
  ('MULTI', 'Multiple answers'),
  ('TEXT', 'Free text answer')
;
COMMIT;

insert into tbl_quiz_status(code, label)
values
  ('DRAFT', 'Draft'),
  ('READY', 'Ready'),
  ('ACTIVE', 'Active'),
  ('COMPLETED', 'Completed')
;
COMMIT;

insert into tbl_user_role(code, label)
values
  ('ROLE_USER', 'Common user'),
  ('ROLE_ADMIN', 'Admin user')
;
COMMIT;