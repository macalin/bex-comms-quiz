DROP TABLE tbl_user_answer;
DROP TABLE tbl_user_quiz;
DROP TABLE tbl_question_statistic;

DROP TABLE tbl_correct_answer;
DROP TABLE tbl_answer;
DROP TABLE tbl_question;
DROP TABLE tbl_question_type;

DROP TABLE tbl_quiz;
DROP TABLE tbl_quiz_status;

DROP TABLE tbl_user;
DROP TABLE tbl_user_role;