angular.module("tab-active", [])
    .config(function($stateProvider){
        $stateProvider
            .state('manage.active', {
                url: "/active",
                template: "<tab-active></tab-active>"
            })
    })
    .component('tabActive', {
        templateUrl: "admin/tab-active/tab-active.html",
        controller: function($scope, $http, $state, ModalService, CounterService, AppConst) {
            var ctrl = this;
            ctrl.calcScores = function(quizId) {
                $http.get('/' + AppConst.restContextPath + "/admin/active/quiz/" + quizId + "/compute")
                    .then(function(response) {
                        $scope.$emit('changeSelectedTab', 3);
                        $state.go('^.completed');
                    }, function(response) {
                        ModalService.openErrorModal("Failed to compute scores!");
                    });
            };

            $http.get('/' + AppConst.restContextPath + "/admin/active/quiz/list")
                .then(function(response) {
                    ctrl.quizzes = response.data;
                    for (var i = 0; i < ctrl.quizzes.length; i++) {
                        var quiz = ctrl.quizzes[i];
                        CounterService.startCounting(quiz);
                    }
                    if ($scope.$parent.activeTabIndex != 2) {
                        $scope.$emit('changeSelectedTab', 2);
                    }
                }, function(response) {
                    ModalService.openErrorModal("Failed to retrieve active quizzes!");
                });
        }
    });