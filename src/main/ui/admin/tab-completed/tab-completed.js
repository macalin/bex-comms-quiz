angular.module("tab-completed", [])
    .config(function($stateProvider){
        $stateProvider
            .state('manage.completed', {
                url: "/completed",
                template: "<tab-completed></tab-completed>"
            })
    })
    .component('tabCompleted', {
        templateUrl: "admin/tab-completed/tab-completed.html",
        controller: function($scope, $http, $state, ModalService, AppConst) {
            var ctrl = this;
            ctrl.contextPath = AppConst.restContextPath;

            ctrl.showScores = function(quizId) {
                $state.go('scores',{"quizId" : quizId});
            };

            ctrl.showStatistics = function(quizId) {
                $state.go('statistics',{"quizId" : quizId});
            };

            $http.get('/' + AppConst.restContextPath + "/admin/completed/quiz/list")
                .then(function(response) {
                    ctrl.quizzes = response.data;
                    if ($scope.$parent.activeTabIndex != 3) {
                        $scope.$emit('changeSelectedTab', 3);
                    }
                }, function(response) {
                    ModalService.openErrorModal("Failed to retrieve completed quizzes!");
                });

        }
    });