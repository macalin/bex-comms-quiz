'use strict';
angular.module("quiz.admin", ["quiz.common", "ui.router", "manage-quizzes", "edit-quiz", "ui.bootstrap", "scores", "statistics", "download-service"]);