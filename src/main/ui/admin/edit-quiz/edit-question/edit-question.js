angular.module("edit-question", ["edit-answer"])
    .component('editQuestion', {
        templateUrl: 'admin/edit-quiz/edit-question/edit-question.html',
        bindings: {
            question: "<"
        },
        controller: function ($scope) {
            var ctrl = this;
            var answerGenId = 1;

            ctrl.save = function () {
            };

            ctrl.addAnswer = function () {
                ctrl.question.answers.push({"id": ctrl.getNextAnswerId()});
            };

            ctrl.getNextAnswerId = function (){
                // This generated answer Id is only used on client side
                // in order to track the selected answers
                // It will not be sent to server
                answerGenId++;
                return "new_"+answerGenId;
            };

            ctrl.removeAnswer = function (answer) {
                var answers = ctrl.question.answers;
                var index = answers.indexOf(answer);
                answers.splice(index, 1);
            };

            ctrl.onQuestionTypeChange = function () {
                var questionType = ctrl.question.type;
                if (questionType === 'single') {
                    ctrl.invalidateCorrectAnswers(ctrl.question.answers);
                }
                if (questionType === 'multi') {
                    ctrl.invalidateCorrectAnswers(ctrl.question.answers);
                }
                if (questionType === 'text') {
                    // remove possible answers
                    ctrl.question.answers = [];
                }
            };

            ctrl.invalidateCorrectAnswers = function (answers) {
                angular.forEach(answers, function (answer, key) {
                    answer.selected = false;
                });
            };

            $scope.$on('removeAnswerEvent', function (event, answer) {
                ctrl.removeAnswer(answer);
            });


        }
    });