angular.module("edit-answer", [])
    .component('editAnswer', {
        templateUrl: 'admin/edit-quiz/edit-question/edit-answer/edit-answer.html',
        bindings: {
            answer: "=",
            questionType: "<",
            singleChoice: "="
        },
        controller: function EditAnswerController($scope) {
            var ctrl = this;

            ctrl.deleteAnswer = function (answer) {
                $scope.$emit('removeAnswerEvent', answer);
            };
        }
    });
