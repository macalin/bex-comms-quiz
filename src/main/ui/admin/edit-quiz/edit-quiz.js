angular.module("edit-quiz", ["edit-question"])
    .config(function($stateProvider){
        $stateProvider
            .state('edit', {
                url: "/edit/:quizId",
                template: "<edit-quiz></edit-quiz>"
            })
    })
    .component('editQuiz', {
        templateUrl: "admin/edit-quiz/edit-quiz.html",
        controller: function($scope, $element, $stateParams, $http, ModalService, AppConst) {
            var ctrl = this;

            ctrl.addNewQuestion = function() {
                ctrl.questions.push(
                    {'type' : "single", "text": "", "answers" : []}
                );
                $scope.currentQuestionNb = ctrl.questions.length-1;
            };

            ctrl.goToQuestion = function(nb) {
                $scope.currentQuestionNb = nb;
            };

            ctrl.validate = function() {
                ctrl.errors = [];
                if (!ctrl.quiz || !ctrl.quiz.name || ctrl.quiz.name.trim() === "") {
                    ctrl.errors.push("Quiz name must not be empty.");
                }
                if (!ctrl.quiz || !ctrl.quiz.duration) {
                    ctrl.errors.push("Quiz duration must not be empty.");
                }
                if (!!ctrl.quiz && !!ctrl.quiz.duration && isNaN(parseInt(ctrl.quiz.duration))) {
                    ctrl.errors.push("Quiz duration must be a number.");
                }
                if (!ctrl.questions || ctrl.questions.length == 0) {
                    ctrl.errors.push("The quiz has no questions.");
                }
                angular.forEach(ctrl.questions, function (question, key) {
                    var questionKey = key + 1;
                    if (!question.text || question.text.trim() === "") {
                        ctrl.errors.push("The question " + questionKey +  " has no text.");
                    }
                    if (question.type == "text") {
                        if (!question.answerText || question.answerText.trim() === "") {
                            ctrl.errors.push("The answer for question " + questionKey + " has no text.");
                        }
                    } else if (question.type == "single" || question.type == "multi") {
                        if (!question.answers || question.answers.length <= 1) {
                            ctrl.errors.push("The question " + questionKey +  " must have minimum two answers.");
                        }
                        angular.forEach(question.answers, function (answer, key) {
                            var answerKey = key + 1;
                            if (!answer.text || answer.text.trim() === "") {
                                ctrl.errors.push("The answer " + answerKey + " for question " + questionKey + " has no text.");
                            }
                        });
                        if (question.type == "single") {
                            if (!question.singleChoice) {
                                ctrl.errors.push("The correct answer is not selected for question " + questionKey +  ".");
                            }
                        } else if (question.type == "multi") {
                            var selectedAnswers = false;
                            for (var i = 0; i < question.answers.length; i++) {
                                if (!!question.answers[i].selected) {
                                    selectedAnswers = true;
                                    return;
                                }
                            }
                            if (!selectedAnswers) {
                                ctrl.errors.push("The correct answers are not selected for question " + questionKey +  ".");
                            }
                        }
                    }
                });
                return ctrl.errors.length == 0;
            };

            ctrl.saveQuiz = function() {
                if (!ctrl.validate()) {
                    ModalService.openErrorModal(ctrl.errors);
                    return false;
                }
                var quizVo = ctrl.getQuizVo();
                $http.post('/' + AppConst.restContextPath + "/admin/draft/quiz", quizVo)
                    .then(function (response) {
                        ctrl.quiz.id = response.data;
                        ModalService.openSuccessModal("Quiz saved with success!");
                    }, function () {
                        ModalService.openErrorModal("Failed to save quiz!");
                    });
            };

            ctrl.getQuizVo = function () {
                var quizVo = ctrl.quiz;
                quizVo.questions = [];
                angular.forEach(ctrl.questions, function (question, key) {
                    var questionVo = {};
                    questionVo.id = question.id;
                    questionVo.text = question.text;
                    questionVo.type = question.type;
                    if (question.type === "single" || question.type === "multi") {
                        questionVo.answers = ctrl.getQuestionAnswers(question);
                    }
                    questionVo.answerText =  question.answerText;
                    quizVo.questions.push(questionVo);
                });
                return quizVo;
            };

            ctrl.getQuestionAnswers = function (question) {
                var answers = [];
                angular.forEach(question.answers, function (answer, key) {
                    var answerVo = {};
                    answerVo.text = answer.text;
                    if (typeof answer.id == "number"){
                        answerVo.id = answer.id;
                    }
                    if (question.type === 'multi'){
                        answerVo.selected = answer.selected;
                    } else {
                        // question.type === 'single'
                        if (question.singleChoice == answer.id) {
                            answerVo.selected = true;
                        }
                    }
                    answers.push(answerVo);
                });
                return answers;
            };

            ctrl.onRemoveQuestion = function() {
                ModalService.openConfirmationModal({
                    messages: "Are you sure you want to remove current question?",
                    okHandler: ctrl.removeQuestion
                });
            };

            ctrl.removeQuestion = function () {
                var question = ctrl.questions[$scope.currentQuestionNb]
                var questions = ctrl.questions;
                var index = questions.indexOf(question);
                questions.splice(index, 1);
                if ($scope.currentQuestionNb > questions.length-1) {
                    $scope.currentQuestionNb --;
                }
                if (questions.length == 0 ) {
                    // add empty question
                    ctrl.addNewQuestion();
                }
            };

            ctrl.computeSingleChoiceModel = function(){
                angular.forEach(ctrl.questions, function(question, key) {
                    if (question.type === 'single'){
                        angular.forEach(question.answers, function(answer, key) {
                            if (answer.selected){
                                question.singleChoice = '' + answer.id;
                            }
                        });
                    }
                });
            };

            ctrl.createNewQuiz = function(){
                $scope.currentQuestionNb = 0;
                ctrl.questions = [];
                ctrl.addNewQuestion();
            };

            ctrl.loadQuiz = function(){

                $http.get('/' + AppConst.restContextPath + "/admin/draft/quiz/"+$stateParams.quizId )
                    .then(function(response) {
                        ctrl.onQuizLoaded(response.data);
                    }, function(response) {
                        ModalService.openErrorModal("Failed to retrieve quiz!");
                    });

            };

            ctrl.onQuizLoaded = function(quizData){
                ctrl.quiz = {id: quizData.id, name : quizData.name, duration: quizData.duration};
                ctrl.questions = quizData.questions;
                ctrl.computeSingleChoiceModel();
                $scope.currentQuestionNb = 0;
            };

            if ($stateParams.quizId){
                // make a call to retrieve the quiz
                ctrl.loadQuiz();
            } else {
                ctrl.createNewQuiz();
            }
        }

});