angular.module("manage-quizzes", ['ui.bootstrap', "tab-draft", "tab-ready", "tab-active", "tab-completed", "tab-administrators"])
    .config(function($stateProvider){
        $stateProvider
            .state('manage', {
                url: "/manage",
                template: "<manage></manage>"
            })
    })
    .component('manage', {
        templateUrl: "admin/manage-quizzes/manage-quizzes.html",
        controller: function($scope) {
            var ctrl = this;
            $scope.$on('changeSelectedTab', function(event, index) {
                $scope.activeTabIndex = index;
            });
            ctrl.doLogout = function () {
                $scope.$emit('logoutEvent');
            };
        }
});