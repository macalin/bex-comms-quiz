angular.module("statistics", [])
    .config(function($stateProvider){
        $stateProvider
            .state('statistics', {
                url: "/statistics/:quizId",
                template: "<statistics></statistics>"
            })
    })
    .component('statistics', {
        templateUrl: "admin/statistics/statistics.html",
        controller: function($http, $stateParams, ModalService, AppConst, DownloadService) {
            var ctrl = this;

            $http.get('/' + AppConst.restContextPath + "/admin/completed/quiz/" + $stateParams.quizId + "/statistics")
                .then(function(response) {
                    ctrl.model = response.data;
                }, function(response) {
                    ModalService.openErrorModal("Failed to retrieve statistics!");
                });

            ctrl.downloadStatistics = function() {
                $http.get('/' + AppConst.restContextPath + "/admin/report/quiz/" + $stateParams.quizId + "/statistics")
                    .then(function(response) {
                        DownloadService.createCsvFile(response, ctrl.model.quizName + "_Statistic");
                    }, function(response) {
                        ModalService.openErrorModal("Failed to retrieve statistics file!");
                    })
            };

            ctrl.downloadQuizQA = function() {
                var req = {
                     method: 'GET',
                     url: '/' + AppConst.restContextPath + "/admin/report/quiz/pdf/" + $stateParams.quizId,
                     headers: { 'Accept': 'application/pdf' },
                     responseType: 'arraybuffer'
                }
                $http(req)
                    .then(function(response) {
                        DownloadService.createPdfFile(response, ctrl.model.quizName);
                    }, function(response) {
                        ModalService.openErrorModal("Failed to retrieve quiz report file!");
                    })
            };

        }
    });