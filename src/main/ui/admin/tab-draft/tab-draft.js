angular.module("tab-draft", [])
    .config(function($stateProvider){
        $stateProvider
            .state('manage.draft', {
                url: "/draft",
                template: "<tab-draft></tab-draft>"
            })
    })
    .component('tabDraft', {
        templateUrl: "admin/tab-draft/tab-draft.html",
        controller: function($scope, $http, $state, ModalService, AppConst) {
            var ctrl = this;

            ctrl.addQuiz = function() {
                $state.go('edit', {"quizId" : null});
            };

            ctrl.editQuiz = function(quizId) {
                $state.go('edit',{"quizId" : quizId});
            };

            ctrl.onDeleteQuiz = function(quizId){
                ModalService.openConfirmationModal({
                    messages: "Are you sure you want to delete the quiz?",
                    okHandler: ctrl.deleteQuiz,
                    okHandlerParams: [quizId]
                });
            };

            ctrl.deleteQuiz = function(quizId) {
                $http.delete('/' + AppConst.restContextPath + "/admin/draft/quiz/" + quizId)
                    .then(function() {
                        ctrl.load();
                    }, function() {
                        ModalService.openErrorModal("Failed to delete quiz!");
                    });
            };

            ctrl.setReady = function(quizId) {
                $http.post('/' + AppConst.restContextPath + "/admin/draft/quiz/" + quizId + "/ready")
                    .then(function() {
                        $scope.$emit('changeSelectedTab', 1);
                        $state.go('^.ready');
                    }, function() {
                        ModalService.openErrorModal("Failed to mark quiz as ready!");
                    });
            };

            ctrl.load = function() {
                $http.get('/' + AppConst.restContextPath + "/admin/draft/quiz/list")
                    .then(function(response) {
                        ctrl.quizzes = response.data;
                        if ($scope.$parent.activeTabIndex != 0) {
                            $scope.$emit('changeSelectedTab', 0);
                        }
                    }, function() {
                        ModalService.openErrorModal("Failed to retrieve quizzes!");
                    });
            };

            ctrl.load();

        }
    });