angular.module("tab-administrators", [])
    .config(function($stateProvider){
        $stateProvider
            .state('manage.administrators', {
                url: "/administrators",
                template: "<tab-administrators></tab-administrators>"
            })
    })
    .component('tabAdministrators', {
        templateUrl: "admin/tab-administrators/tab-administrators.html",
        controller: function($scope, $http, $state, ModalService, AppConst) {
            var ctrl = this;
            ctrl.contextPath = AppConst.restContextPath;

            ctrl.addAdministrator = function(username) {
                $http.post('/' + AppConst.restContextPath + "/admin/administrator", username)
                    .then(function(response) {
                       ctrl.loadAdministrators();
                    }, function(response) {
                        ModalService.openErrorModal("Failed to add administrator!");
                    });
            }

            ctrl.removeAdministrator = function(username) {
                $http.post('/' + AppConst.restContextPath + "/admin/administrator/remove", username)
                    .then(function(response) {
                       ctrl.loadAdministrators();
                    }, function(response) {
                        ModalService.openErrorModal("Failed to remove administrator!");
                    });
            }

           ctrl.loadAdministrators = function() {
                $http.get('/' + AppConst.restContextPath + "/admin/administrator/list")
                    .then(function(response) {
                        ctrl.administrators = response.data;
                        if ($scope.$parent.activeTabIndex != 4) {
                            $scope.$emit('changeSelectedTab', 4);
                        }
                    }, function(response) {
                        ModalService.openErrorModal("Failed to retrieve administrators!");
                    });
           }

            ctrl.loadAdministrators();

        }
    });