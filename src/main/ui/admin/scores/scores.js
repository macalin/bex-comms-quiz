angular.module("scores", [])
    .config(function($stateProvider){
        $stateProvider
            .state('scores', {
                url: "/scores/:quizId",
                template: "<scores></scores>"
            })
    })
    .component('scores', {
        templateUrl: "admin/scores/scores.html",
        controller: function($http, $stateParams, ModalService, AppConst, DownloadService) {
            var ctrl = this;

            $http.get('/' + AppConst.restContextPath + "/admin/completed/quiz/" + $stateParams.quizId + "/quizScores")
                .then(function(response) {
                    ctrl.model = response.data;
                }, function(response) {
                    ModalService.openErrorModal("Failed to retrieve scores!");
                });

            ctrl.downloadHighScorers = function() {
                $http.get('/' + AppConst.restContextPath + "/admin/report/quiz/" + $stateParams.quizId + "/highScorers")
                    .then(function(response) {
                        DownloadService.createCsvFile(response, ctrl.model.quizName + "_High_Scorers");
                    }, function(response) {
                        ModalService.openErrorModal("Failed to retrieve high scorers file!");
                    })
            };

            ctrl.downloadScores = function() {
                $http.get('/' + AppConst.restContextPath + "/admin/report/quiz/" + $stateParams.quizId + "/scores")
                    .then(function(response) {
                        DownloadService.createCsvFile(response, ctrl.model.quizName + "_Scores");
                    }, function(response) {
                        ModalService.openErrorModal("Failed to retrieve Scores file!");
                    })
            };

        }
    });