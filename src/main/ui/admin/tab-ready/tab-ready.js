angular.module("tab-ready", ['ngStomp'])
    .config(function($stateProvider){
        $stateProvider
            .state('manage.ready', {
                url: "/ready",
                template: "<tab-ready></tab-ready>"
            })
    })
    .component('tabReady', {
        templateUrl: "admin/tab-ready/tab-ready.html",
        controller: function($scope, $http, $state, ModalService, AppConst, $stomp) {
            var ctrl = this;

            $http.get('/' + AppConst.restContextPath + "/admin/ready/quiz/list")
                .then(function(response) {
                    ctrl.quizzes = response.data;
                    if ($scope.$parent.activeTabIndex != 1) {
                        $scope.$emit('changeSelectedTab', 1);
                    }
                }, function(response) {
                    ModalService.openErrorModal("Failed to retrieve quizzes!");
                });

            ctrl.setDraft = function(quizId) {
                $http.post('/' + AppConst.restContextPath + "/admin/ready/quiz/" + quizId + "/draft")
                    .then(function(response) {
                        $scope.$emit('changeSelectedTab', 0);
                        $state.go('^.draft');
                    }, function(response) {
                        ModalService.openErrorModal("Failed to set to draft!");
                    });
            };

            ctrl.onSetActive = function(quizId) {
                ModalService.openConfirmationModal({
                    messages: "Are you sure you want to activate the quiz?",
                    okHandler: ctrl.setActive,
                    okHandlerParams: [quizId]
                });
            };

            ctrl.setActive = function(quizId) {
                $http.post('/' + AppConst.restContextPath + "/admin/ready/quiz/" + quizId + "/active")
                    .then(function(response) {
                        $scope.$emit('changeSelectedTab', 2);
                        $state.go('^.active');
                        ctrl.notifyActiveQuizzes();
                    }, function(response) {
                        ModalService.openErrorModal("Failed to activate quiz!");
                    });
            };

            ctrl.notifyActiveQuizzes = function() {
                $stomp.connect('/' + AppConst.contextPath + '/ws', {})
                    .then(function (frame) {
                        $stomp.send('/app/new');
                        $stomp.disconnect();
                    }, function(error){
                        console.error(error);
                    })
            };

        }
    });