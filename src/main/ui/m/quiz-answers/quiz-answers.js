angular.module("quiz-answers", ["question-answer-view", "quiz-pager"])
    .config(function($stateProvider){
        $stateProvider
            .state('answers', {
                url: "/answers",
                params: {
                    quizId: null
                },
                template: "<quiz-answers></quiz-answers>"
            })
    })
    .component('quizAnswers', {
        templateUrl: 'm/quiz-answers/quiz-answers.html',
        controller: function ($scope, $http, $stateParams, ModalService, AppConst) {
            var ctrl = this;
            ctrl.loading = true;
            $scope.questions = [];
            $scope.currentQuestionNumber = 0;
            ctrl.startQuiz = function (quizData) {
                ctrl.userName = quizData.userName;
                ctrl.questions = quizData.questions;
                ctrl.computeSingleChoiceModel();
                ctrl.loading = false;
            };
            ctrl.computeSingleChoiceModel = function(){
                angular.forEach(ctrl.questions, function(question, key) {
                    if (question.type === 'single'){
                        angular.forEach(question.answers, function(answer, key) {
                            if (answer.selected){
                                question.singleChoice = '' + answer.id;
                            }
                        });
                    }
                });
            };

            ctrl.onInfoClick = function(){
                ModalService.openAnswerInfoModal();
            };

            $scope.$on('changeQuestion', function (event, questionNb) {
                $scope.currentQuestionNumber = questionNb;
            });
            $http.get('/' + AppConst.restContextPath + '/quiz/answers?quizId=' + $stateParams.quizId)
                .then(function (response) {
                    ctrl.startQuiz(response.data);
                }, function (response) {
                    if (response.status === 401) {
                        // go to login page
                        $scope.$emit('logoutEvent');
                        return;
                    }
                    ModalService.openErrorModal("Failed to retrieve questions!");
                });
        }
    });