angular.module("question-answer-view", [])
    .component('questionAnswerView', {
        templateUrl: 'm/quiz-answers/question-answer-view/question-answer-view.html',
        bindings: {
            question: '<'
        },
        controller: function() {
            var ctrl = this;
            ctrl.getIconClass = function(index) {
                var answer = ctrl.question.answers[index];
                if (answer.selected && answer.correct) {
                    return "glyphicon glyphicon-ok-sign";
                } else if (answer.selected && !answer.correct) {
                    return "glyphicon glyphicon-remove-sign";
                } else if (!answer.selected && answer.correct) {
                    return "glyphicon glyphicon-exclamation-sign text-danger";
                }
                return "glyphicon glyphicon-none";
            };

            ctrl.getAnswerClass = function(index) {
                var answer = ctrl.question.answers[index];
                if (answer.correct) {
                    return "alert alert-success";
                } else if (answer.selected && !answer.correct) {
                    return "alert alert-danger";
                }
                return "";
            };
        }
    });