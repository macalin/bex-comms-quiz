angular.module("quiz-pager", [])
    .component('quizPager', {
        templateUrl: 'm/quiz-view/quiz-pager/quiz-pager.html',
        bindings:{
            currentQuestionNumber:"<",
            numberOfQuestions:"<",
            readOnly: "<"
        },
        controller: function ($scope, $state) {
            var ctrl = this;
            ctrl.goTo = function (nb) {
                $scope.$emit('changeQuestion', nb);
            };

            ctrl.isFirstQuestion = function() {
                return ctrl.currentQuestionNumber  < 1;
            };

            ctrl.isLastQuestion = function() {
                return ctrl.currentQuestionNumber >= ctrl.numberOfQuestions - 1;
            };

            ctrl.isSubmitVisible = function() {
                return !ctrl.readOnly && ctrl.isLastQuestion();
            };

            ctrl.submitQuiz = function (nb) {
                $scope.$emit('changeQuestion', nb);
                $state.go('score');
            };

        }
    });