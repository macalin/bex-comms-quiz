angular.module("question-view", [])
    .component('questionView', {
        templateUrl: 'm/quiz-view/question-view/question-view.html',
        bindings: {
            question: '<'
        }
    });
