angular.module("quiz-view", ["question-view", "quiz-pager"])
    .config(function($stateProvider){
        $stateProvider
            .state('quiz', {
                url: "/quiz",
                params: {
                    quizId: null
                },
                template: "<quiz-view></quiz-view>"
            })
    })
    .component('quizView', {
        templateUrl: 'm/quiz-view/quiz-view.html',
        controller: function ($scope, $http, $interval, $state, $stateParams, ModalService, CounterService, AppConst) {

            var stop;
            var ctrl = this;
            ctrl.loading = true;
            $scope.questions = [];
            $scope.currentQuestionNumber = 0;
            ctrl.startQuiz = function (quizData) {

                ctrl.userName = quizData.userName;
                ctrl.questions = quizData.questions;
                ctrl.computeSingleChoiceModel();

                stop = CounterService.startCounting(quizData, ctrl.stopQuiz);
                ctrl.loading = false;
            };
            ctrl.stopQuiz = function () {
                if (angular.isDefined(stop)) {
                    $interval.cancel(stop);
                    stop = undefined;
                }
                ctrl.submitQuiz();
            };
            ctrl.submitQuiz = function (nb) {
                $scope.$emit('changeQuestion', nb);
                $state.go('score');
            };

            ctrl.computeSingleChoiceModel = function () {
                angular.forEach(ctrl.questions, function (question, key) {
                    if (question.type === 'single') {
                        angular.forEach(question.answers, function (answer, key) {
                            if (answer.selected) {
                                question.singleChoice = '' + answer.id;
                            }
                        });
                    }
                });
            };

            ctrl.getUserAnswer = function () {
                var question = ctrl.questions[$scope.currentQuestionNumber];
                var userAnswer = {questionId: question.id};
                if (question.type === 'text') {
                    userAnswer.answerText = question.answerText;
                } else if (question.type === 'single' && !!question.singleChoice) {
                    userAnswer.selectedAnswerIds = [question.singleChoice];
                } else {
                    var selectedValues = [];
                    angular.forEach(question.answers, function (answer, key) {
                        if (answer.selected) {
                            selectedValues.push(answer.id);
                        }
                    });
                    userAnswer.selectedAnswerIds = selectedValues;
                }
                return userAnswer;
            };

            ctrl.persistUserAnswer = function () {
                var userAnswer = ctrl.getUserAnswer();
                return $http.post('/' + AppConst.restContextPath + '/quiz/user/answer', userAnswer);
            };

            $scope.$on('changeQuestion', function (event, questionNb) {
                ctrl.persistUserAnswer().then(function (response) {
                    $scope.currentQuestionNumber = questionNb;
                }, function (response) {
                    ModalService.openErrorModal("Failed to submit user response");
                });
            });

            this.doLogout = function () {
                $scope.$emit('logoutEvent');
            };

            $http.get('/' + AppConst.restContextPath + '/quiz?quizId=' + $stateParams.quizId)
                // use mocks URL
                //$http.get('/static/quiz/resources/quiz.json')
                .then(function (response) {
                    ctrl.quiz = response.data;
                    ctrl.startQuiz(response.data);
                }, function (response) {
                    if (response.status === 401) {
                        // go to login page
                        $scope.$emit('logoutEvent');
                        return;
                    }
                    ModalService.openErrorModal("Failed to retrieve questions");
                });

        }
    });