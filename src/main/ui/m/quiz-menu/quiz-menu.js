angular.module("quiz-menu", ['ngStomp'])
    .config(function($stateProvider){
        $stateProvider
            .state('menu', {
                url: "/menu",
                template: "<quiz-menu></quiz-menu>"
            })
    })
    .component('quizMenu', {
        templateUrl: 'm/quiz-menu/quiz-menu.html',
        controller: function ($scope, $http, ModalService, CounterService, AppConst, $stomp) {
            var ctrl = this;
            var subscription;

            ctrl.loading = true;
            ctrl.quizzes = [];

            ctrl.doLogout = function () {
                $scope.$emit('logoutEvent');
            };

            ctrl.loadQuizzes = function(){
                $http.get('/' + AppConst.restContextPath + "/quiz/active")
                    .then(function (response) {
                        ctrl.quizzes = response.data;
                        for (var i = 0; i < ctrl.quizzes.length; i++) {
                            var quiz = ctrl.quizzes[i];
                            if (quiz.status === 'ACTIVE') {
                                CounterService.startCounting(quiz);
                            };
                        }
                        ctrl.loading = false;
                    }, function (response) {
                        ModalService.openErrorModal("Failed to retrieve quizzes!");
                    });
            };

            ctrl.$onDestroy = function() {
                subscription.unsubscribe();
                $stomp.disconnect();
            }

            ctrl.loadQuizzes();

            $stomp.connect('/'+ AppConst.contextPath + '/ws',{})
                .then(function(frame) {
                    subscription = $stomp.subscribe('/topic/quiz', function() {
                        ctrl.loadQuizzes();
                    }, {});
                }, function(error){
                    console.error(error);
                });
        }
    });
