angular.module("score-view", [])
    .config(function($stateProvider){
        $stateProvider
            .state('score', {
                url: "/score",
                template: "<score-view></score-view>"
            })
    })
    .component('scoreView', {
        templateUrl: 'm/score-view/score-view.html',
        controller: function ($scope, $state) {
            var ctrl = this;
            this.doLogout = function () {
                $scope.$emit('logoutEvent');
            };
            this.goStart = function () {
                $state.go('menu');
            };
        }
    });