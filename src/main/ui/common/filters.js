"use strict";
angular.module("application")
  .filter('secondsToMmSs', ['$filter', function($filter) {
    return function(seconds) {
        if (seconds > 3600) {
            return $filter('date')(new Date(0, 0, 0).setSeconds(seconds), 'hh:mm:ss');
        }
        return $filter('date')(new Date(0, 0, 0).setSeconds(seconds), 'mm:ss');
    };
}]);