angular.module("application")
 .factory('httpRequestInterceptor',
    ['$state', '$window', '$q', function($state, $window, $q){
        return {
            request: function($config) {
                $config.headers = $config.headers || {};
                if ($window.localStorage.token) {
                    $config.headers['user-token'] = 'Bearer ' + $window.localStorage.token;
                }
                return $config;
            },
            responseError: function (response) {
                var status = response.status;
                if (status == 401) {
                    $window.localStorage.removeItem("token");
                    $window.localStorage.removeItem("isAdmin");
                    $state.go('unauthorized', {errMsg: response.headers("errMsg")});
                    return;
                }
                // otherwise
                return $q.reject(response);
            }
        };
    }]
 );