angular.module("download-service", [])
    .service("DownloadService", function() {
        var service = this;

        this.createCsvFile = function(response, filename) {
            var csvContent = response.data;
            var fullFileName = filename+'.csv'
            // IE10+
            if (navigator.msSaveBlob) {
                return navigator.msSaveBlob(
                    new Blob([csvContent], {type: 'application/csv'})
                    , fullFileName
                );
            }
            angular.element('<a/>').attr({
                href: 'data:attachment/csv;charset=utf-8,' + encodeURIComponent(csvContent),
                target: '_self' ,
                download: fullFileName
            })[0].click();
        };

        this.createPdfFile = function(response, filename) {
            var fullFileName = filename+'.pdf';
            var blob = new Blob([response.data], {type: 'application/pdf'});

            // IE10+
            if (navigator.msSaveBlob) {
                return navigator.msSaveBlob(
                    blob
                    , fullFileName
                );
            }

            var fileURL = URL.createObjectURL(blob);
            angular.element('<a/>').attr({
                href: fileURL,
                target: '_self' ,
                download: fullFileName
            })[0].click();
        };
    });