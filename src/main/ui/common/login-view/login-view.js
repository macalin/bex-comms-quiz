angular.module("login-view", ['ui.router'])
    .config(function($stateProvider){
        $stateProvider
            .state('login', {
                url: "/login?isAdmin&token",
                template: '<login-view></login-view>',
                resolve: {
                    nextState : function($location, $window, $http, ModalService, AppConst) {
                    	
                    	var that = this;
                    	that.getNextState = function(){
                            if (!!$window.localStorage.getItem("token")) {
                            	// check admin or normal user
                                if ($window.localStorage.getItem("isAdmin") === "true") {
                                    return 'manage.draft';
                                } else {
                                    return 'menu';
                                }
                            } else {
                                return 'login';
                            }
                        };
                        
                    	 var userToken = $location.search().token;
                         var isAdmin = $location.search().isAdmin;
                         if (!!userToken) {
                             $window.localStorage.setItem('token', userToken);
                             $window.localStorage.setItem('isAdmin', isAdmin);
                             var resp = $http.post('/' + AppConst.restContextPath + "/login/user");
                             return resp.then(function() {
                                	 return that.getNextState();
                                 }, function() {
                                     ModalService.openErrorModal("Failed to update / login user!");
                                     return 'login';
                                 });
                         } else {
                             $window.localStorage.removeItem("token");
                             $window.localStorage.removeItem("isAdmin");
                        	 return 'login';
                         }
                      },
                    }
            })
    })
    .component('loginView', {
        templateUrl: 'common/login-view/login-view.html',
        controller: function ($rootScope, $scope, $window, $state, $http, ModalService, AppConst) {
            var ctrl = this;
            
            ctrl.onGetToken = function (username) {

            	//check if user exists on server
                $http.get('/' + AppConst.contextPath + "/login/user/clause?username=" + username)
                    .then(function (response) {
                    	if (response.data.userExists === false ){
                    		ModalService.openConfirmationModal({
                    		    templateUrl: "common/login-view/terms-and-conditions-modal.html",
                                messages: response.data.clause,
                                okHandler: ctrl.getToken,
                                okHandlerParams: [username]
                            });
                    	} else {
                    		ctrl.getToken(username);
                    	}
                    }, function (response) {
                        ModalService.openErrorModal("Failed to create user token");
                    });
            };

            ctrl.getToken = function (username) {

                $http.get('/' + AppConst.contextPath + "/login/user?username=" + username)
                    .then(function (response) {
                        ModalService.openSuccessModal("A token was generated. Please check your e-mail.");
                    }, function (response) {
                        ModalService.openErrorModal("Failed to create user token");
                    });
            };

            ctrl.removeToken = function() {
                $window.localStorage.removeItem("token");
                $window.localStorage.removeItem("isAdmin");
            };
            
            ctrl.nextState = $scope.$parent.$resolve.nextState;
            
            if (ctrl.nextState !== "login"){
            	$state.go(ctrl.nextState);
            	// register logout event
            	$rootScope.$on('logoutEvent', function (event) {
                     ctrl.removeToken();
                     $state.go('login');
                 });
            }
           
        }
        
});