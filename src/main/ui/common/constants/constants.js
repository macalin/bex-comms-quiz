angular.module('constants', [])
    .constant('AppConst',
        {
            contextPath:              window.location.pathname.substring(1, window.location.pathname.indexOf("/",2)),
            restContextPath:          window.location.pathname.substring(1, window.location.pathname.indexOf("/",2)) + '/rest',
            loginUrl:                 window.location.href.substring(0,window.location.href.indexOf('/index.html')+ "/index.html".length) + "#/login"
        }
);