angular.module("modal-service",["ui.bootstrap", "answer-info"])
    .service("ModalService", function ($uibModal) {
        var service = this;
        this.openSuccessModal = function (messages) {
            return $uibModal.open({
                templateUrl: service.getTemplateUrl(),
                animation: true,
                backdrop: false,
                controller: service.ModalInstanceCtrl,
                resolve: {
                    data: {
                        title: "Success",
                        headerClass: "alert alert-success",
                        messages: service.toArray(messages),
                        confirmButton: "Ok",
                        okIcon: 'glyphicon-ok'
                    }
                }
            });
        };

        this.openAnswerInfoModal = function (messages) {
            return $uibModal.open({
                templateUrl: service.getTemplateUrl(),
                animation: true,
                backdrop: false,
                controller: service.ModalInstanceCtrl,
                resolve: {
                    data: {
                        title: "Info",
                        isAnswerModal: true,
                        headerClass: "alert alert-info",
                        confirmButton: "Ok",
                        okIcon: 'glyphicon-ok'
                    }
                }
            });
        };

        this.openConfirmationModal = function (options) {
            var modalTemplate = options.templateUrl != null ? options.templateUrl : service.getTemplateUrl();
            return $uibModal.open({
                templateUrl: modalTemplate,
                animation: true,
                backdrop: false,
                controller: service.ModalInstanceCtrl,
                resolve: {
                    data: {
                        title: "Confirmation",
                        headerClass: "alert alert-info",
                        messages: service.toArray(options.messages),
                        confirmButton: "Confirm",
                        cancelButton: "Cancel",
                        okIcon: 'glyphicon-ok',
                        cancelIcon: 'glyphicon-remove',
                        okHandler: options.okHandler,
                        okHandlerParams : options.okHandlerParams
                    }
                }
            });
        };

        this.openWarningModal = function (messages) {
            return $uibModal.open({
                templateUrl: service.getTemplateUrl(),
                animation: true,
                backdrop: false,
                controller: service.ModalInstanceCtrl,
                resolve: {
                    data: {
                        title: "Warning",
                        headerClass: "alert alert-warning",
                        messages: service.toArray(messages),
                        confirmButton: "Close",
                        okIcon: 'glyphicon-remove'
                    }
                }
            });
        };

        this.openErrorModal = function (messages) {
            return $uibModal.open({
                templateUrl: service.getTemplateUrl(),
                animation: true,
                backdrop: false,
                controller: service.ModalInstanceCtrl,
                resolve: {
                    data: {
                        title: "Error",
                        headerClass: "alert alert-danger",
                        messages: service.toArray(messages),
                        confirmButton: "Close",
                        okIcon: 'glyphicon-remove'
                    }
                }
            });
        };

        this.getTemplateUrl = function () {
            return "common/modal/modal-content.html";
        };

        this.toArray = function (messages) {
            return typeof messages == 'string' ? [messages] : messages;
        };

        this.ModalInstanceCtrl = function ($scope, $uibModalInstance, data) {
            $scope.data = data;

            $scope.ok = function () {
                $uibModalInstance.close(data);
                if (data.okHandler){
                    data.okHandler.apply(null, data.okHandlerParams);
                }
            };

            $scope.cancel = function () {
                $uibModalInstance.dismiss("cancel");
            };
        }
    });
