angular.module("counter-service",[])
    .service("CounterService", function ($interval) {
        this.startCounting = function (currentQuiz, onCountEndHandler){

            var quizTimeRemaining = currentQuiz.remainingSeconds * 1000;
            var onTick = function (currentQuiz) {
                var millisUntilEnd = currentQuiz.endTime - new Date().getTime();
                if (millisUntilEnd < 0) {
                    if (onCountEndHandler){
                        onCountEndHandler();
                    }
                    return;
                }
                currentQuiz.remainingSeconds = Math.floor(millisUntilEnd / 1000);
            };
            currentQuiz.endTime = new Date().getTime() + quizTimeRemaining;

            return $interval(onTick, 100, 0, true, currentQuiz);
        };
    });
