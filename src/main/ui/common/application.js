"use strict";
angular.module("application", [
    "quiz.admin",
    "quiz.m",
    "constants",
    "templates",
    "app.common.directives"
    ])
    .config(function($httpProvider, $urlRouterProvider){
        // Initialize get if not there
        if (!$httpProvider.defaults.headers.get) {
            $httpProvider.defaults.headers.get = {};
        }
        // Disable IE ajax request caching
        $httpProvider.defaults.headers.get['If-Modified-Since'] = '0';
        // add http interceptor
        $httpProvider.interceptors.push('httpRequestInterceptor');

        // For any unmatched url, send to /
        $urlRouterProvider.otherwise("/login");
    })
    .run(function () {
    })
    .component("application", {
        templateUrl: "common/application.html"
    }
);

