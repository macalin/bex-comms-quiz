angular.module("app.common.directive.ngClickDebounced", [])
    .directive("ngClickDebounced", function() {
       return {
            restrict: "A",
            priority: 1,
            link: function (scope, element, attr) {
                var time = 1000;
                element.bind("click", _.debounce(function() {
                    scope.$apply(attr.ngClickDebounced);
                }, time));
            }
       }
    });