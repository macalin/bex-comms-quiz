angular.module("unauthorized", ['ui.router'])
	.config(function($stateProvider){
		$stateProvider
			.state('unauthorized', {
				url: "/unauthorized",
				params: { errMsg: ""},
				template:"<unauthorized></unauthorized>"
			})
	})
	.component('unauthorized', {
		templateUrl : 'common/login-view/login-view.html',
		controller  : function($state, ModalService){
			var errorMessage = $state.params.errMsg;
			if (errorMessage.length < 1){
				errorMessage = "You are not authorized. Please get a token.";
			}
			ModalService.openErrorModal(errorMessage)
				.closed.then(function(){
					$state.go('login');
				})
		} 
	})