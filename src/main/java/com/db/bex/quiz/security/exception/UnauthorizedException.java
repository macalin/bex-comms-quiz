package com.db.bex.quiz.security.exception;

/**
 * Created by Andrei on 20/01/2017.
 */
public class UnauthorizedException extends RuntimeException {

    public UnauthorizedException (String message) {
        super(message);
    }

}
