package com.db.bex.quiz.security;

/**
 * Created by cergale on 04/07/2016.
 */
import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.db.bex.quiz.model.UserRoleCode;
import com.db.bex.quiz.utils.TokenGenerator;
import io.jsonwebtoken.ExpiredJwtException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.filter.GenericFilterBean;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.SignatureException;

public class JwtFilter extends GenericFilterBean {

    private static final Logger LOG = LoggerFactory.getLogger(JwtFilter.class);
    
    private static final String ERR_MSG_HEADER = "errMsg";

    public void doFilter(final ServletRequest req,
                         final ServletResponse res,
                         final FilterChain chain) throws IOException, ServletException {
        final HttpServletRequest request = (HttpServletRequest) req;
        final HttpServletResponse response = (HttpServletResponse) res;

        final String userToken = request.getHeader("user-token");

        if (userToken == null || !userToken.startsWith("Bearer ")) {
        	response.setHeader(ERR_MSG_HEADER, "You are not authorized. Please get a token.");
            response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Missing or invalid Authorization header.");
            return;
        }

        try {
            final Claims claims = TokenGenerator.getTokenClaims(userToken.substring(7));
            checkRole(claims, request);
            request.setAttribute("claims", claims);
        } catch (final SignatureException e) {
            LOG.error(e.getLocalizedMessage());
            response.setHeader(ERR_MSG_HEADER, "Invalid token. Please get a new one.");
            response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Invalid token.");
            return;
        } catch (final ExpiredJwtException e) {
            LOG.error(e.getLocalizedMessage());
            response.setHeader(ERR_MSG_HEADER, "Your token has expired. Please get a new one.");
            response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Expired token.");
            return;
        }

        chain.doFilter(req, res);
    }

    public void checkRole(Claims claims, HttpServletRequest request){
        String role = (String)claims.get("role");
        String pathWithinApplication = request.getRequestURI().substring(request.getContextPath().length());
        if (UserRoleCode.ADMIN.getCode().equalsIgnoreCase(role)){
            if (pathWithinApplication.startsWith("/rest/admin")) {
                return;
            }
        }
        if (UserRoleCode.USER.getCode().equalsIgnoreCase(role)){
            if (pathWithinApplication.startsWith("/rest/quiz")) {
                return;
            }
        }
        if (pathWithinApplication.startsWith("/rest/login")) {
            return;
        }
        
        throw new SignatureException("Invalid token for requested resource:" +
                " user role: "+ role + "requested resource "+pathWithinApplication);
    }

}