package com.db.bex.quiz.web.service.impl;

import com.db.bex.quiz.dao.QuizDao;
import com.db.bex.quiz.dao.UserDao;
import com.db.bex.quiz.model.*;
import com.db.bex.quiz.web.service.PdfReportService;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.ByteArrayOutputStream;

/**
 * Created by Andrei on 30/01/2017.
 */
@Service
public class PdfReportServiceImpl implements PdfReportService {

    private static final Logger LOG = LoggerFactory.getLogger(PdfReportServiceImpl.class);

    private static Font titleFont = new Font(Font.FontFamily.TIMES_ROMAN, 18, Font.BOLD);

    private static Font questionFont = new Font(Font.FontFamily.TIMES_ROMAN, 14);

    private static Font correctAnswerFont = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD);

    private static Font answerFont = new Font(Font.FontFamily.TIMES_ROMAN, 12);

    @Autowired
    private QuizDao quizDao;

    @Autowired
    private UserDao userDao;

    @Transactional
    @Override
    public byte[] getQuizQAndA(Integer quizId, String username) {
        byte [] result = null;
        Document document = null;
        try(ByteArrayOutputStream outB = new ByteArrayOutputStream()) {
            document = new Document();
            PdfWriter.getInstance(document, outB);
            document.open();
            Quiz quiz = quizDao.findById(quizId);
            User user = userDao.findByUsername(username);

            addMetaData(document, quiz, user);
            document.add(getTitle(quiz));
            document.add(getContent(quiz));

            document.close();
            result = outB.toByteArray();
        } catch (Exception e) {
            throw new RuntimeException("Error while creating pdf byte array!", e);
        }
        return result;
    }

    private void addMetaData(Document document, Quiz quiz, User user) {
        document.addTitle(quiz.getName() + " Q&A Report");
        document.addSubject(quiz.getName());
        document.addAuthor(user.getFirstName() + " " + user.getLastName());
        document.addCreator("Comms Quiz");
    }

    private Element getTitle(Quiz quiz) throws DocumentException {
        Paragraph preface = new Paragraph();

        Paragraph title = new Paragraph(quiz.getName() + " Q & A", titleFont);
        title.setAlignment(Element.ALIGN_CENTER);
        preface.add(title);

        addEmptyLine(preface, 3);

        return preface;
    }

    private void addEmptyLine(Paragraph paragraph, int number) {
        for (int i = 0; i < number; i++) {
            paragraph.add(new Paragraph(" "));
        }
    }

    private Element getContent(Quiz quiz) throws DocumentException {
        List questionList = new List(true, false, 10);
        questionList.setAutoindent(true);
        for (Question question: quiz.getQuestions()) {
            questionList.add(new ListItem(question.getText(), questionFont));
            questionList.add(getQuestionAnswers(question));
            questionList.add(new Paragraph(" "));
        }
        return questionList;
    }

    private Element getQuestionAnswers(Question question) {
        Element element = null;
        if (QuestionType.SINGLE.equals(question.getType().getCode()) || QuestionType.MULTI.equals(question.getType().getCode())) {
            List answerList = new List(false, true, 10);
            answerList.setLowercase(true);
            answerList.setAutoindent(true);
            for (Answer answer : question.getAnswers()) {
                if (answer.getCorrectAnswer() != null) {
                    answerList.add(new ListItem(answer.getText(), correctAnswerFont));
                } else {
                    answerList.add(new ListItem(answer.getText(), answerFont));
                }
            }
            element = answerList;
        } else if (QuestionType.TEXT.equals(question.getType().getCode())) {
            List answerList = new List(false, false, 10);
            answerList.add(new ListItem(question.getCorrectAnswers().get(0).getText(), correctAnswerFont));
            element = answerList;
        }
        return element;
    }

}