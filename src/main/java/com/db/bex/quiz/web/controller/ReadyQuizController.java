package com.db.bex.quiz.web.controller;

import com.db.bex.quiz.vo.QuizVo;
import com.db.bex.quiz.web.service.AdminQuizService;
import io.jsonwebtoken.Claims;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by calimih on 18/07/2016.
 */
@Controller
@RequestMapping("/rest/admin/ready/quiz")
public class ReadyQuizController {

    @Autowired
    AdminQuizService quizService;

    @RequestMapping(value = "/list", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<List<QuizVo>> getActiveQuizzes() {
        List<QuizVo> draftQuizzes = quizService.findReadyQuizzes();
        return new ResponseEntity<List<QuizVo>>(draftQuizzes, HttpStatus.OK);
    }

    @RequestMapping(value = "/{quizId}/draft", method = RequestMethod.POST)
    public ResponseEntity markQuizAsDraft(HttpServletRequest request, @PathVariable("quizId") int quizId) {
        Claims claims = (Claims)request.getAttribute("claims");
        quizService.markQuizAsDraft(claims.getSubject(), quizId);
        return new ResponseEntity(HttpStatus.OK);
    }

    @RequestMapping(value = "/{quizId}/active", method = RequestMethod.POST)
    public ResponseEntity markQuizAsActive(HttpServletRequest request, @PathVariable("quizId") int quizId) {
        Claims claims = (Claims)request.getAttribute("claims");
        quizService.startTimerAndMarkQuizAsActive(claims.getSubject(), quizId);
        return new ResponseEntity(HttpStatus.OK);
    }

}
