package com.db.bex.quiz.web.controller;

import com.db.bex.quiz.web.service.PdfReportService;
import io.jsonwebtoken.Claims;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * Created by calimih on 26/09/2016.
 */
@Controller
@RequestMapping("/rest/admin/report/quiz/pdf")
public class PdfReportController {

    private static final Logger LOG = LoggerFactory.getLogger(PdfReportController.class);

    @Autowired
    PdfReportService reportService;

    @RequestMapping(value = "/{quizId}", method = RequestMethod.GET, produces = "application/pdf")
    public HttpEntity<byte[]> getQuiz(@PathVariable int quizId, HttpServletRequest request) throws IOException {
        Claims claims = (Claims)request.getAttribute("claims");
        byte[] documentBody = reportService.getQuizQAndA(quizId, claims.getSubject());
        return getHttpEntity(quizId, documentBody);
    }

    private HttpEntity<byte[]> getHttpEntity(int quizId, byte[] documentBody){
        HttpHeaders header = new HttpHeaders();
        header.setContentType(new MediaType("application", "pdf"));
        header.set("Content-Disposition",
                "attachment; filename=" + quizId + ".pdf");
        header.setContentLength(documentBody.length);

        return new HttpEntity<>(documentBody, header);
    }

}
