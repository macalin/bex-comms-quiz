package com.db.bex.quiz.web.service;


import com.db.bex.quiz.vo.QuizVo;

import java.util.List;

/**
 * Created by cergale on 12/07/2016.
 */
public interface AdminQuizService {

    List<QuizVo> findDraftQuizzes();

    List<QuizVo> findReadyQuizzes();

    List<QuizVo> findActiveQuizzes();

    List<QuizVo> findCompletedQuizzes();

    QuizVo findQuiz(int id);

    Integer saveQuiz(String username, QuizVo quizVo);

    void deleteQuiz(Integer quizId);

    void markQuizAsReady(String username, Integer quizId);

    void markQuizAsDraft(String username, Integer quizId);

    void startTimerAndMarkQuizAsActive(String username, Integer quizId);

    void computeScoresAndMarkQuizAsCompleted(String username, Integer quizId);

}
