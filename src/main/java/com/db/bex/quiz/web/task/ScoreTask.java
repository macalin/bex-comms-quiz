package com.db.bex.quiz.web.task;

import com.db.bex.quiz.web.service.AdminQuizService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by cergale on 05/10/2016.
 */
@Component
@Scope("prototype")
public  class ScoreTask extends TimerTask {

    @Autowired
    AdminQuizService quizService;

    private Integer quizId;
    private String quizName;
    private String username;
    private Timer timer;

    private static final Logger LOG = LoggerFactory.getLogger(ScoreTask.class);

    public void setParams(Integer quizId, String quizName, String username, Timer timer){
        this.quizId = quizId;
        this.quizName = quizName;
        this.username = username;
        this.timer = timer;
    }

    public void run() {
        LOG.info("Time is up for quiz "+ quizName + " with id " + quizId +" !  Start calculating scores.");
        quizService.computeScoresAndMarkQuizAsCompleted(username, quizId);
        LOG.info("Finished calculating scores for quiz "+ quizName + " with id " + quizId +".");
        timer.cancel();
    }
}