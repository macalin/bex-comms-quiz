package com.db.bex.quiz.web.service.impl;

import com.db.bex.quiz.dao.QuestionStatisticDao;
import com.db.bex.quiz.dao.QuizDao;
import com.db.bex.quiz.dao.UserQuizDao;
import com.db.bex.quiz.model.*;
import com.db.bex.quiz.model.utils.SortByQuestion;
import com.db.bex.quiz.vo.QuizScoreVo;
import com.db.bex.quiz.vo.QuizStatisticsVo;
import com.db.bex.quiz.web.service.ScoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;
import java.util.stream.Collectors;

import static com.db.bex.quiz.model.QuestionType.*;

/**
 * Created by calimih on 18/07/2016.
 */
@Service
public class ScoreServiceImpl implements ScoreService {

    private static final long ONE_HUNDRED = 100;

    @Autowired
    UserQuizDao userQuizDao;

    @Autowired
    QuizDao quizDao;

    @Autowired
    QuestionStatisticDao questionStatisticDao;

    @Transactional
    public void computeQuizScores(Integer quizId) {
        List<UserQuiz> userQuizzes =  userQuizDao.findByQuizId(quizId);
        Map<Integer, Integer> questionStatistic = new HashMap<>();
        for (UserQuiz userQuiz : userQuizzes) {
            computeUserQuizScore(userQuiz.getUser().getUsername(), userQuiz.getQuiz().getId(), questionStatistic);
        }
        Quiz quiz = quizDao.findById(quizId);
        for (Question question: quiz.getQuestions()) {
            QuestionStatistic statistic = new QuestionStatistic();
            statistic.setQuestion(question);
            BigDecimal percentage = BigDecimal.ZERO;
            if (questionStatistic.containsKey(question.getId())) {
                percentage = (new BigDecimal(questionStatistic.get(question.getId())).divide(new BigDecimal(userQuizzes.size()), 4, RoundingMode.HALF_EVEN));
            }
            statistic.setCorrectAnswersPercentage(percentage.doubleValue());
            questionStatisticDao.saveQuestionStatistic(statistic);
        }
    }

    @Transactional
    public void computeUserQuizScore(String username, Integer quizId, Map<Integer, Integer> questionStatistic) {
        UserQuiz userQuiz = userQuizDao.findByUsernameAndQuizId(username, quizId);
        List<UserAnswer> userAnswers = userQuiz.getUserAnswers();
        Collections.sort(userAnswers, new SortByQuestion());
        short score = 0;
        int index = 0;
        while (index < userAnswers.size()) {
            UserAnswer userAnswer = userAnswers.get(index);

            List<CorrectAnswer> correctAnswers = userAnswer.getQuestion().getCorrectAnswers();
            QuestionType questionType = userAnswer.getQuestion().getType();
            if (SINGLE.equals(questionType.getCode())) {
                if (userAnswer.getAnswer().equals(correctAnswers.get(0).getAnswer())) {
                    score++;
                    computeQuestionStatistic(questionStatistic, userAnswer.getQuestion().getId());
                }
            } else if (TEXT.equals(questionType.getCode())) {
                if (isSameTextAnswer(userAnswer.getAnswerText(), correctAnswers.get(0).getText())) {
                    score++;
                    computeQuestionStatistic(questionStatistic, userAnswer.getQuestion().getId());
                }
            } else if (MULTI.equals(questionType.getCode())) {
                Question currentQuestion = userAnswer.getQuestion();
                Set<Answer> userQuestionAnswers = new HashSet<>();
                userQuestionAnswers.add(userAnswer.getAnswer());
                while (index + 1 < userAnswers.size() && userAnswers.get(index + 1).getQuestion().equals(currentQuestion)) {
                    index++;
                    userAnswer = userAnswers.get(index);
                    userQuestionAnswers.add(userAnswer.getAnswer());
                }
                Set<Answer> correctAnswersSet = getCorrectAnswers(correctAnswers);
                if (userQuestionAnswers.equals(correctAnswersSet)) {
                    score++;
                    computeQuestionStatistic(questionStatistic, userAnswer.getQuestion().getId());
                }
            } else {
                throw new IllegalArgumentException();
            }
            index++;
        }
        userQuiz.setScore(score);
        userQuizDao.saveUserQuiz(userQuiz);
    }
    
    public boolean isSameTextAnswer(String answer1, String answer2){
    	if (answer1 == null && answer2 == null){
    		return true;
    	}
    	if (answer1 == null || answer2 == null){
    		return false;
    	}
    	return answer1.trim().equalsIgnoreCase(answer2.trim());
    }

    private void computeQuestionStatistic(Map<Integer, Integer> questionStatistic, Integer questionId) {
        if (questionStatistic.containsKey(questionId)) {
            questionStatistic.put(questionId, questionStatistic.get(questionId) + 1);
        } else {
            questionStatistic.put(questionId, 1);
        }
    }

    private Set<Answer> getCorrectAnswers(List<CorrectAnswer> correctAnswers) {
        Set<Answer> answers = new HashSet<Answer>();
        for (CorrectAnswer correctAnswer : correctAnswers) {
            answers.add(correctAnswer.getAnswer());
        }
        return answers;
    }

    @Transactional
    public QuizScoreVo getQuizScores(Integer quizId) {
        QuizScoreVo vo = new QuizScoreVo();
        Quiz quiz = quizDao.findById(quizId);
        vo.setQuizName(quiz.getName());
        Integer quizQuestionsNo = quiz.getQuestions().size();
        List<QuizScoreVo.UserScoreVo> userScores = new ArrayList<QuizScoreVo.UserScoreVo>();
        for (UserQuiz userQuiz : userQuizDao.findByQuizId(quizId)) {
            userScores.add(convertToUserScoreVo(userQuiz, quizQuestionsNo));
        }
        vo.setUserScores(userScores);
        return vo;
    }

    @Transactional
    public QuizStatisticsVo getQuizStatistics(Integer quizId) {
        QuizStatisticsVo vo = new QuizStatisticsVo();
        Quiz quiz = quizDao.findById(quizId);
        vo.setQuizName(quiz.getName());
        List<QuizStatisticsVo.QuestionStatisticVo> questionStatistics = new ArrayList<>();
        questionStatistics.addAll(questionStatisticDao.findByQuizId(quizId).stream()
                .map(questionStatistic -> convertToQuestionStatisticVo(questionStatistic))
                .collect(Collectors.toList()));
        QuizStatisticsVo.QuestionStatisticVo average = new QuizStatisticsVo.QuestionStatisticVo();
        average.setQuestion("Quiz Average");
        average.setPercentage(questionStatisticDao.findByQuizId(quizId).stream()
                .mapToDouble(QuestionStatistic::getCorrectAnswersPercentage)
                .average().getAsDouble() * ONE_HUNDRED);
        questionStatistics.add(average);
        vo.setQuestionStatistics(questionStatistics);
        return vo;
    }

    private QuizStatisticsVo.QuestionStatisticVo convertToQuestionStatisticVo(QuestionStatistic questionStatistic) {
        QuizStatisticsVo.QuestionStatisticVo questionStatisticVo = new QuizStatisticsVo.QuestionStatisticVo();
        questionStatisticVo.setQuestion(questionStatistic.getQuestion().getText());
        questionStatisticVo.setPercentage(questionStatistic.getCorrectAnswersPercentage() * ONE_HUNDRED);
        return questionStatisticVo;
    }

    private QuizScoreVo.UserScoreVo convertToUserScoreVo(UserQuiz userQuiz, Integer quizQuestionsNo) {
        QuizScoreVo.UserScoreVo userScoreVo = new QuizScoreVo.UserScoreVo();
        userScoreVo.setFirstName(userQuiz.getUser().getFirstName());
        userScoreVo.setLastName(userQuiz.getUser().getLastName());
        userScoreVo.setScore(percentage(userQuiz.getScore(), quizQuestionsNo));
        return userScoreVo;
    }

    private Double percentage(Short part, Integer total) {
        return part / (double)total * ONE_HUNDRED;
    }

}
