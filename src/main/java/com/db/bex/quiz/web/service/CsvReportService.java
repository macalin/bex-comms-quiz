package com.db.bex.quiz.web.service;

/**
 * Created by calimih on 27/09/2016.
 */
public interface CsvReportService {

    /**
     * Creates CSV Scores report byte array
     * @param quizId
     * @return report byte array
     */
    byte[] getScores(Integer quizId);

    /**
     * Creates CSV High Scorers report byte array
     * @param quizId
     * @return report byte array
     */
    byte[] getHighScorers(Integer quizId);

    /**
     * Creates CSV statistic report byte array
     * @param quizId
     * @return report byte array
     */
    byte[] getStatistics(Integer quizId);

}
