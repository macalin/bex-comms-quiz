package com.db.bex.quiz.web.controller;

import com.db.bex.quiz.vo.AdministratorVo;
import com.db.bex.quiz.web.service.AdministratorService;
import io.jsonwebtoken.Claims;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by Andrei on 13/01/2017.
 */
@Controller
@RequestMapping("/rest/admin/administrator")
public class AdministratorsController {

    @Autowired
    AdministratorService administratorService;

    @RequestMapping(value = "/list", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<List<AdministratorVo>> getAdministrators(HttpServletRequest request) {
        Claims claims = (Claims)request.getAttribute("claims");
        List<AdministratorVo> administrators = administratorService.loadAdministrators(claims.getSubject());
        return new ResponseEntity(administrators, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity addAdministrator(@RequestBody String username) {
        administratorService.addAdministrator(username);
        return new ResponseEntity(HttpStatus.OK);
    }

    @RequestMapping(value = "/remove", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity removeAdministrator(HttpServletRequest request, @RequestBody String username) {
        Claims claims = (Claims) request.getAttribute("claims");
        administratorService.removeAdministrator(claims.getSubject(), username);
        return new ResponseEntity(HttpStatus.OK);
    }

}
