package com.db.bex.quiz.web.service;

import com.db.bex.quiz.vo.QuizScoreVo;
import com.db.bex.quiz.vo.QuizStatisticsVo;

/**
 * Created by calimih on 18/07/2016.
 */
public interface ScoreService {

    void computeQuizScores(Integer quizId);

    QuizScoreVo getQuizScores(Integer quizId);

    QuizStatisticsVo getQuizStatistics(Integer quizId);
    
    boolean isSameTextAnswer(String answer1, String answer2);

}
