package com.db.bex.quiz.web.service;

/**
 * Created by Andrei on 30/01/2017.
 */
public interface PdfReportService {

    /**
     * Creates PDF Quiz report byte array
     * @param quizId
     * @param username
     * @return PDF report byte array
     */
    byte[] getQuizQAndA(Integer quizId, String username);

}
