package com.db.bex.quiz.web.controller;

import com.db.bex.quiz.vo.QuizVo;
import com.db.bex.quiz.web.service.AdminQuizService;
import io.jsonwebtoken.Claims;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by cergale on 12/07/2016.
 */
@Controller
@RequestMapping("/rest/admin/draft/quiz")
public class DraftQuizController {


    @Autowired
    AdminQuizService quizService;

    @RequestMapping(value = "/list", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<List<QuizVo>> getDraftQuizzes() {
        List<QuizVo> draftQuizzes = quizService.findDraftQuizzes();
        return new ResponseEntity<List<QuizVo>>(draftQuizzes, HttpStatus.OK);
    }

    @RequestMapping(value = "/{quizId}", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<QuizVo> getQuiz(@PathVariable("quizId") int quizId) {
        QuizVo quizVo = quizService.findQuiz(quizId);
        return new ResponseEntity<QuizVo>(quizVo, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    public ResponseEntity<Integer> saveQuiz(HttpServletRequest request, @RequestBody QuizVo quizVo) {
        Claims claims = (Claims)request.getAttribute("claims");
        Integer quizId = quizService.saveQuiz(claims.getSubject(), quizVo);
        return new ResponseEntity(quizId, HttpStatus.OK);
    }

    @RequestMapping(value = "/{quizId}", method = RequestMethod.DELETE)
    public ResponseEntity<QuizVo> deleteQuiz(@PathVariable("quizId") int quizId) {
        quizService.deleteQuiz(quizId);
        return new ResponseEntity(HttpStatus.OK);
    }

    @RequestMapping(value = "/{quizId}/ready", method = RequestMethod.POST)
    public ResponseEntity markQuizAsReady(HttpServletRequest request, @PathVariable("quizId") int quizId) {
        Claims claims = (Claims)request.getAttribute("claims");
        quizService.markQuizAsReady(claims.getSubject(), quizId);
        return new ResponseEntity(HttpStatus.OK);
    }

}