package com.db.bex.quiz.web.controller;

import com.db.bex.quiz.vo.QuizVo;
import com.db.bex.quiz.web.service.AdminQuizService;
import io.jsonwebtoken.Claims;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by calimih on 18/07/2016.
 */
@Controller
@RequestMapping("/rest/admin/active/quiz")
public class ActiveQuizController {

    @Autowired
    AdminQuizService quizService;

    @RequestMapping(value = "/list", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<List<QuizVo>> getActiveQuizzes(HttpServletRequest request) {
        List<QuizVo> activeQuizzes = quizService.findActiveQuizzes();
        return new ResponseEntity<List<QuizVo>>(activeQuizzes, HttpStatus.OK);
    }

    @RequestMapping(value = "/{quizId}/compute", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<QuizVo> computeQuizScores(HttpServletRequest request, @PathVariable int quizId) {
        Claims claims = (Claims)request.getAttribute("claims");
        quizService.computeScoresAndMarkQuizAsCompleted(claims.getSubject(), quizId);
        return new ResponseEntity(HttpStatus.OK);
    }

}
