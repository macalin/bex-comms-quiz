package com.db.bex.quiz.web.service;

import com.db.bex.quiz.vo.AdministratorVo;

import java.util.List;

/**
 * Created by Andrei on 13/01/2017.
 */
public interface AdministratorService {

    List<AdministratorVo> loadAdministrators(String loggedUser);

    void removeAdministrator(String loggedUser, String username);

    void addAdministrator(String username);

}
