package com.db.bex.quiz.web.report;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.List;

/**
 * Created by calimih on 25/09/2016.
 */
public class CsvWriter {

    private static final Logger LOG = LoggerFactory.getLogger(CsvWriter.class);

    //Delimiter used in CSV file
    private static final String NEW_LINE_SEPARATOR = System.getProperty("line.separator");

    public static byte[] getCSVByteArray( List<List<String>> rows) {

        byte [] result = null;
        final ByteArrayOutputStream outB = new ByteArrayOutputStream();
        CSVPrinter csvPrinter = null;
        try(Writer out = new BufferedWriter(new OutputStreamWriter(outB))) {
            CSVFormat csvFileFormat = CSVFormat.DEFAULT.withRecordSeparator(NEW_LINE_SEPARATOR);

            csvPrinter = new CSVPrinter(out, csvFileFormat);
            for (List<String> row : rows) {
                csvPrinter.printRecord(row);
            }
            csvPrinter.flush();
            result = outB.toByteArray();

        } catch (IOException e) {
            throw new RuntimeException("Error while creating csv byte array!", e);
        }  finally {
            try {
                csvPrinter.close();
                outB.close();
            } catch (IOException e) {
                throw new RuntimeException("Error while flushing/closing outputstream /csvPrinter!", e);
            }
        }
        return result;
    }

}
