package com.db.bex.quiz.web.service.impl;

import com.db.bex.quiz.dao.*;
import com.db.bex.quiz.model.*;
import com.db.bex.quiz.vo.AnswerVo;
import com.db.bex.quiz.vo.QuestionVo;
import com.db.bex.quiz.vo.QuizVo;
import com.db.bex.quiz.web.service.AdminQuizService;
import com.db.bex.quiz.web.service.ScoreService;
import com.db.bex.quiz.web.task.ScoreTask;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.function.Predicate;

/**
 * Created by cergale on 12/07/2016.
 */

@Service("adminQuizService")
@Transactional
public class AdminQuizServiceImpl implements AdminQuizService {

    @Autowired
    ScoreService scoreService;

    @Autowired
    private ObjectFactory<ScoreTask> scoreTaskFactory;

    @Autowired
    QuizDao quizDao;

    @Autowired
    QuizStatusDao quizStatusDao;

    @Autowired
    QuestionDao questionDao;

    @Autowired
    QuestionTypeDao questionTypeDao;

    @Autowired
    AnswerDao answerDao;

    @Autowired
    CorrectAnswerDao correctAnswerDao;

    @Autowired
    UserDao userDao;

    public List<QuizVo> findDraftQuizzes() {
        return findQuizzesByStatus(QuizStatus.DRAFT);
    }

    public List<QuizVo> findReadyQuizzes() {
        return findQuizzesByStatus(QuizStatus.READY);
    }

    public List<QuizVo> findActiveQuizzes() {
        List<QuizVo> activeQuizzes = findQuizzesByStatus(QuizStatus.ACTIVE);
        for (QuizVo quiz : activeQuizzes) {
            Long passedTime = TimeUnit.MILLISECONDS.toSeconds((new Date()).getTime() - quiz.getDateStarted().getTime());
            Long remainingTime = TimeUnit.MINUTES.toSeconds(quiz.getDuration()) - passedTime;
            quiz.setRemainingSeconds(remainingTime);
        }
        return activeQuizzes;
    }

    public List<QuizVo> findCompletedQuizzes() {
        return findQuizzesByStatus(QuizStatus.COMPLETED);
    }

    public void markQuizAsReady(String username, Integer quizId) {
        changeQuizStatus(username, quizId, QuizStatus.READY);
    }

    public void markQuizAsDraft(String username, Integer quizId) {
        changeQuizStatus(username, quizId, QuizStatus.DRAFT);
    }

    public void startTimerAndMarkQuizAsActive(String username, Integer quizId) {
        Quiz quiz = quizDao.findById(quizId);
        quiz.setDateStarted(new Date());
        quiz.setUserUpdated(userDao.findByUsername(username));
        quiz.setStatus(quizStatusDao.findByCode(QuizStatus.ACTIVE));
        quizDao.saveQuiz(quiz);

        final Timer timer = new Timer();
        ScoreTask scoreTask = scoreTaskFactory.getObject();
        scoreTask.setParams( quizId, quiz.getName(), username, timer);
        timer.schedule(scoreTask, TimeUnit.MINUTES.toMillis(quiz.getDuration()));
    }

    public void computeScoresAndMarkQuizAsCompleted(String username, Integer quizId) {
        scoreService.computeQuizScores(quizId);
        changeQuizStatus(username, quizId, QuizStatus.COMPLETED);
    }

    private void changeQuizStatus(String username, Integer quizId, String quizStatus) {
        Quiz quiz = quizDao.findById(quizId);
        quiz.setUserUpdated(userDao.findByUsername(username));
        quiz.setStatus(quizStatusDao.findByCode(quizStatus));
        quizDao.saveQuiz(quiz);
    }

    public List<QuizVo> findQuizzesByStatus(String quizStatus) {
        List<QuizVo> quizzes = new ArrayList<QuizVo>();
        List<Quiz> modelQuizzes = quizDao.findQuizzesByStatus(quizStatus);
        for (Quiz model : modelQuizzes) {
            quizzes.add(convertModelToVo(model));
        }
        return quizzes;
    }

    public QuizVo findQuiz(int id) {
        QuizVo quizVo = new QuizVo();
        Quiz quiz = quizDao.findById(id);

        quizVo.setId(quiz.getId());
        quizVo.setName(quiz.getName());
        quizVo.setDuration(quiz.getDuration());

        List<QuestionVo> questions = new ArrayList<QuestionVo>();
        for (Question question : quiz.getQuestions()) {
            QuestionVo questionVo = new QuestionVo();
            questionVo.setId(question.getId());
            questionVo.setType(question.getType().getCode().toLowerCase());
            questionVo.setText(question.getText());

            List<CorrectAnswer> correctAnswers = question.getCorrectAnswers();
            Set<Integer> correctAnswerIds = getCorrectAnswerIds(correctAnswers);

            List<AnswerVo> answers = new ArrayList<AnswerVo>();
            for (Answer answer : question.getAnswers()) {
                AnswerVo answerVo = new AnswerVo();
                answerVo.setId(answer.getId());
                answerVo.setText(answer.getText());
                if (correctAnswerIds.contains(answer.getId())){
                    answerVo.setSelected(true);
                }
                answers.add(answerVo);
            }
            questionVo.setAnswers(answers);
            if (QuestionType.TEXT.equals(question.getType().getCode())) {
                CorrectAnswer cAnswer = correctAnswers.get(0);
                if (cAnswer != null){
                    questionVo.setAnswerText(cAnswer.getText());
                }
            }

            questions.add(questionVo);
        }

        quizVo.setQuestions(questions);

        return quizVo;
    }

    public void deleteQuiz(Integer quizId) {
        quizDao.deleteQuiz(quizDao.findById(quizId));
    }

    @Transactional
    public Integer saveQuiz(String username, QuizVo quizVo) {
        User currentUser = userDao.findByUsername(username);
        Quiz quiz;
        if (quizVo.getId() != null) {
            quiz = quizDao.findById(quizVo.getId());
            quiz.setUserUpdated(currentUser);
        } else {
            quiz = new Quiz();
            quiz.setUserCreated(currentUser);
            quiz.setStatus(quizStatusDao.findByCode(QuizStatus.DRAFT));
        }
        quiz.setName(quizVo.getName());
        quiz.setDuration(quizVo.getDuration());
        quizDao.saveQuiz(quiz);
        convertVoToModel(quiz, quizVo, currentUser);
        return quiz.getId();
    }

    private void convertVoToModel(Quiz quiz, QuizVo quizVo, User currentUser) {
        deleteRemovedQuestions(quizVo);
        for (QuestionVo questionVo : quizVo.getQuestions()) {
            Question question;
            if (questionVo.getId() != null) {
                question = questionDao.findById(questionVo.getId());
                question.setUserUpdated(currentUser);
            } else {
                question = new Question();
                question.setUserCreated(currentUser);
            }
            question.setText(questionVo.getText());
            question.setQuiz(quiz);
            question.setType(questionTypeDao.findByCode(questionVo.getType()));
            questionDao.saveQuestion(question);

            deleteRemovedAnswers(questionVo);

            if (QuestionType.TEXT.equals(questionVo.getType().toUpperCase())) {
                updateCorrectAnswer(question, currentUser, questionVo.getAnswerText());
            } else {
                for (AnswerVo answerVo : questionVo.getAnswers()) {
                    Boolean isUpdateAnswer = answerVo.getId() != null;
                    Answer answer;
                    if (isUpdateAnswer) {
                        answer = answerDao.findById(answerVo.getId());
                        answer.setUserUpdated(currentUser);
                    } else {
                        answer = new Answer();
                        answer.setUserCreated(currentUser);
                    }
                    answer.setQuestion(question);
                    answer.setText(answerVo.getText());
                    answerDao.saveAnswer(answer);
                    updateCorrectAnswer(isUpdateAnswer, question, answer, currentUser, answerVo.isSelected());
                }
            }
        }
    }

    private void deleteRemovedQuestions(final QuizVo quizVo) {
        if (quizVo.getId() == null) {
            return;
        }
        List<Question> questions = questionDao.findByQuizId(quizVo.getId());
        questions.removeIf(new Predicate<Question>() {
            public boolean test(Question question) {
                if (quizVo.getQuestions().contains(new QuestionVo(question.getId()))) {
                    return true;
                }
                return false;
            }
        });
        questionDao.deleteQuestions(questions);
    }

    private void deleteRemovedAnswers(final QuestionVo questionVo) {
        if (questionVo.getId() == null) {
            return;
        }
        List<Answer> answers = answerDao.findByQuestionId(questionVo.getId());
        answers.removeIf(new Predicate<Answer>() {
            public boolean test(Answer answer) {
                if (questionVo.getAnswers().contains(new AnswerVo(answer.getId()))) {
                    return true;
                }
                return false;
            }
        });
        answerDao.deleteAnswers(answers);
    }

    private void updateCorrectAnswer(Boolean isUpdateAnswer, Question question, Answer answer, User currentUser, boolean isCorrectAnswer) {
        CorrectAnswer correctAnswer = null;
        if (isUpdateAnswer) {
            correctAnswer = correctAnswerDao.findByQuestionIdAndAnswerId(question.getId(), answer.getId());
        }
        if (isCorrectAnswer) {
            if (correctAnswer != null) {
                correctAnswer.setUserUpdated(currentUser);
            } else {
                correctAnswer = new CorrectAnswer();
                correctAnswer.setUserCreated(currentUser);
            }
            correctAnswer.setQuestion(question);
            correctAnswer.setAnswer(answer);
            correctAnswerDao.saveCorrectAnswer(correctAnswer);
        } else if (correctAnswer != null) {
            correctAnswerDao.deleteCorrectAnswer(correctAnswer);
        }
    }

    private void updateCorrectAnswer(Question question, User currentUser, String correctAnswerText) {
        CorrectAnswer correctAnswer = correctAnswerDao.findByQuestionId(question.getId());
        if (correctAnswer != null) {
            correctAnswer.setUserUpdated(currentUser);
        } else {
            correctAnswer = new CorrectAnswer();
            correctAnswer.setUserCreated(currentUser);
        }
        correctAnswer.setQuestion(question);
        correctAnswer.setAnswer(null);
        correctAnswer.setText(correctAnswerText);
        correctAnswerDao.saveCorrectAnswer(correctAnswer);
    }

    private QuizVo convertModelToVo(Quiz model) {
        QuizVo vo = new QuizVo();
        vo.setId(model.getId());
        vo.setName(model.getName());
        vo.setStatus(model.getStatus().getLabel());
        vo.setDuration(model.getDuration());
        vo.setDateStarted(model.getDateStarted());
        return vo;
    }

    private Set<Integer> getCorrectAnswerIds(List<CorrectAnswer> correctAnswers){
        Set<Integer> correctAnswerIds = new HashSet<Integer>();
        for (CorrectAnswer ca : correctAnswers){
            Answer ans = ca.getAnswer();
            if (ans != null){
                correctAnswerIds.add(ans.getId());
            }
        }
        return correctAnswerIds;
    }
}



