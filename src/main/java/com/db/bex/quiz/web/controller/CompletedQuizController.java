package com.db.bex.quiz.web.controller;

import com.db.bex.quiz.vo.QuizScoreVo;
import com.db.bex.quiz.vo.QuizStatisticsVo;
import com.db.bex.quiz.vo.QuizVo;
import com.db.bex.quiz.web.service.AdminQuizService;
import com.db.bex.quiz.web.service.ScoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by calimih on 18/07/2016.
 */
@Controller
@RequestMapping("/rest/admin/completed/quiz")
public class CompletedQuizController {

    @Autowired
    AdminQuizService quizService;

    @Autowired
    ScoreService scoreService;

    @RequestMapping(value = "/list", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<List<QuizVo>> getActiveQuizzes(HttpServletRequest request) {
        List<QuizVo> draftQuizzes = quizService.findCompletedQuizzes();
        return new ResponseEntity<>(draftQuizzes, HttpStatus.OK);
    }

    @RequestMapping(value = "/{quizId}/quizScores", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<QuizScoreVo> getQuizScores(@PathVariable int quizId) {
        QuizScoreVo quizScores = scoreService.getQuizScores(quizId);
        return new ResponseEntity<>(quizScores, HttpStatus.OK);
    }

    @RequestMapping(value = "/{quizId}/statistics", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<QuizStatisticsVo> getQuizStatistics(@PathVariable int quizId) {
        QuizStatisticsVo quizStatistics = scoreService.getQuizStatistics(quizId);
        return new ResponseEntity<>(quizStatistics, HttpStatus.OK);
    }

}
