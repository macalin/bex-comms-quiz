package com.db.bex.quiz.web.service.impl;

import com.db.bex.quiz.dao.UserDao;
import com.db.bex.quiz.dao.UserRoleDao;
import com.db.bex.quiz.model.User;
import com.db.bex.quiz.model.UserRoleCode;
import com.db.bex.quiz.utils.UserUtils;
import com.db.bex.quiz.vo.AdministratorVo;
import com.db.bex.quiz.web.service.AdministratorService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Andrei on 13/01/2017.
 */
@Service
public class AdministratorServiceImpl implements AdministratorService {

    private static final Logger LOG = LoggerFactory.getLogger(AdministratorServiceImpl.class);

    @Autowired
    UserDao userDao;

    @Autowired
    UserRoleDao userRoleDao;

    private AdministratorVo convertToAdministratorVo(User user, String loggedUser) {
        AdministratorVo administratorVo = new AdministratorVo();
        administratorVo.setName(user.getFirstName() + " " + user.getLastName());
        administratorVo.setUsername(user.getUsername());
        administratorVo.setIsCurrentUser(loggedUser.equals(user.getUsername()));
        return administratorVo;
    }

    @Override
    @Transactional
    public List<AdministratorVo> loadAdministrators(String loggedUser) {
        List<User> administrators = userDao.loadAdministrators();
        return administrators.stream().map(user -> convertToAdministratorVo(user, loggedUser))
                .collect(Collectors.toList());
    }

    @Override
    @Transactional
    public void removeAdministrator(String loggedUser, String username) {
        if (loggedUser.equals(username)) {
            LOG.error("Logged user tried to remove himself from administrators list.");
            return;
        }
        User user = userDao.findByUsername(username);
        user.setUserRole(userRoleDao.findByCode(UserRoleCode.USER.getCode()));
        user.setDateUpdated(new Date());
        userDao.saveUser(user);
    }

    @Override
    @Transactional
    public void addAdministrator(String username) {
        User user = userDao.findByUsername(username);
        if (user == null) {
            user = new User();
            user.setUserRole(userRoleDao.findByCode(UserRoleCode.ADMIN.getCode()));
            user.setUsername(username);
            user.setEmail(UserUtils.getEmail(username));
            user.setFirstName(UserUtils.getFirstName(username));
            user.setLastName(UserUtils.getLastName(username));
            userDao.saveUser(user);
        } else {
            user.setUserRole(userRoleDao.findByCode(UserRoleCode.ADMIN.getCode()));
            user.setDateUpdated(new Date());
        }
    }

}
