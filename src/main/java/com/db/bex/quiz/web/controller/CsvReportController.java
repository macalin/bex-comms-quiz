package com.db.bex.quiz.web.controller;

import com.db.bex.quiz.web.service.CsvReportService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.io.IOException;

/**
 * Created by calimih on 26/09/2016.
 */
@Controller
@RequestMapping("/rest/admin/report/quiz")
public class CsvReportController {

    private static final Logger LOG = LoggerFactory.getLogger(CsvReportController.class);

    @Autowired
    CsvReportService reportService;

    @RequestMapping(value = "/{quizId}/scores", method = RequestMethod.GET)
    public HttpEntity<byte[]> getScores(
            @PathVariable int quizId) throws IOException {

        byte[] documentBody = reportService.getScores(quizId);
        return getHttpEntity(quizId, documentBody);
    }

    @RequestMapping(value = "/{quizId}/highScorers", method = RequestMethod.GET)
    public HttpEntity<byte[]> getHighScores(
            @PathVariable int quizId) throws IOException {

        byte[] documentBody = reportService.getHighScorers(quizId);
        return getHttpEntity(quizId, documentBody);
    }

    @RequestMapping(value = "/{quizId}/statistics", method = RequestMethod.GET)
    public HttpEntity<byte[]> getStatistic(
            @PathVariable int quizId) throws IOException {

        byte[] documentBody = reportService.getStatistics(quizId);
        return getHttpEntity(quizId, documentBody);
    }

    private HttpEntity<byte[]> getHttpEntity(int quizId, byte[] documentBody){
        HttpHeaders header = new HttpHeaders();
        header.setContentType(new MediaType("text", "csv"));
        header.set("Content-Disposition",
                "attachment; filename=" + quizId + ".csv");
        header.setContentLength(documentBody.length);

        return new HttpEntity<>(documentBody, header);
    }

}
