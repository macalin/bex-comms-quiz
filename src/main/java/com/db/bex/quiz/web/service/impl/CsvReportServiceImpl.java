package com.db.bex.quiz.web.service.impl;

import com.db.bex.quiz.dao.QuestionStatisticDao;
import com.db.bex.quiz.dao.QuizDao;
import com.db.bex.quiz.dao.UserQuizDao;
import com.db.bex.quiz.model.UserQuiz;
import com.db.bex.quiz.vo.QuizScoreVo;
import com.db.bex.quiz.vo.QuizStatisticsVo;
import com.db.bex.quiz.web.report.CsvWriter;
import com.db.bex.quiz.web.service.CsvReportService;
import com.db.bex.quiz.web.service.ScoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Created by calimih on 27/09/2016.
 */
@Service
public class CsvReportServiceImpl implements CsvReportService {

    private final static List<String> STATISTIC_HEADER = Arrays.asList("Question", "Correct Answer Percentage");

    private final static List<String> SCORES_HEADER = Arrays.asList("#", "First Name", "Last Name", "Score");

    @Autowired
    QuizDao quizDao;

    @Autowired
    UserQuizDao userQuizDao;

    @Autowired
    QuestionStatisticDao questionStatisticDao;

    @Autowired
    ScoreService scoreService;

    @Transactional
    public byte[] getScores(Integer quizId) {
        List<List<String>> scores = new ArrayList<>();
        scores.add(SCORES_HEADER);
        List<QuizScoreVo.UserScoreVo> userScoreVos = scoreService.getQuizScores(quizId).getUserScores();
        scores.addAll(IntStream.range(0, userScoreVos.size())
                .mapToObj(index -> convertToScoreCvsRow(userScoreVos.get(index), index))
                .collect(Collectors.toList()));
        return CsvWriter.getCSVByteArray(scores);
    }

    @Transactional
    public byte[] getHighScorers(Integer quizId) {
        List<List<String>> highScorers = userQuizDao.findHighScorers(quizId).stream()
                .map(userQuiz -> convertToHighScorerCvsRow(userQuiz))
                .collect(Collectors.toList());
        return CsvWriter.getCSVByteArray(highScorers);
    }

    @Transactional
    public byte[] getStatistics(Integer quizId) {
        List<List<String>> statistics = new ArrayList<>();
        statistics.add(STATISTIC_HEADER);
        statistics.addAll(scoreService.getQuizStatistics(quizId).getQuestionStatistics().stream()
                .map(questionStatistic -> convertToStatisticCvsRow(questionStatistic))
                .collect(Collectors.toList()));
        return CsvWriter.getCSVByteArray(statistics);
    }

    private List<String> convertToStatisticCvsRow(QuizStatisticsVo.QuestionStatisticVo questionStatistic) {
        List<String> row = new ArrayList<>();
        row.add(questionStatistic.getQuestion());
        row.add(getFormattedPercentage(questionStatistic.getPercentage()));
        return row;
    }

    private String getFormattedPercentage(Double value) {
        return new BigDecimal(value).setScale(2, RoundingMode.HALF_EVEN).toString() + "%";
    }

    private List<String> convertToScoreCvsRow(QuizScoreVo.UserScoreVo userScoreVo, int index) {
        List<String> row = new ArrayList<>();
        row.add(String.valueOf(++index));
        row.add(userScoreVo.getFirstName());
        row.add(userScoreVo.getLastName());
        row.add(getFormattedPercentage(userScoreVo.getScore()));
        return row;
    }

    private List<String> convertToHighScorerCvsRow(UserQuiz userQuiz) {
        List<String> row = new ArrayList<>();
        row.add(userQuiz.getUser().getFirstName() + " " + userQuiz.getUser().getLastName());
        return row;
    }

}
