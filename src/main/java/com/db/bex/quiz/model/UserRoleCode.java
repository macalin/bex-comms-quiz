package com.db.bex.quiz.model;

/**
 * Created by cergale on 24/09/2016.
 */
public enum UserRoleCode {

    ADMIN("ROLE_ADMIN"),
    USER("ROLE_USER");

    private String code;

    UserRoleCode(String code){
        this.code = code;
    }

    public String getCode(){
        return code;
    }

}
