package com.db.bex.quiz.model;

import javax.persistence.*;
import java.util.List;

/**
 * Created by calimih on 01/07/2016.
 */
@Entity
@Table(name = "tbl_user_quiz")
public class UserQuiz extends BaseEntity<Integer> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_quiz_id")
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @OneToOne
    @JoinColumn(name = "quiz_id")
    private Quiz quiz;

    @Column(name = "score")
    private Short score;

    @OneToMany
    @JoinColumn(name = "user_quiz_id")
    private List<UserAnswer> userAnswers;

    public User getUser() {
        return user;
    }

    public Short getScore() {
        return score;
    }

    public void setScore(Short score) {
        this.score = score;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<UserAnswer> getUserAnswers() {
        return userAnswers;
    }

    public void setUserAnswers(List<UserAnswer> userAnswers) {
        this.userAnswers = userAnswers;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Quiz getQuiz() {
        return quiz;
    }

    public void setQuiz(Quiz quiz) {
        this.quiz = quiz;
    }

}
