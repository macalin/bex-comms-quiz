package com.db.bex.quiz.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * Created by calimih on 29/06/2016.
 */
@Entity
@Table(name = "tbl_quiz_status")
public class QuizStatus {

    public final static String DRAFT = "DRAFT";
    public final static String READY = "READY";
    public final static String ACTIVE = "ACTIVE";
    public final static String COMPLETED = "COMPLETED";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "quiz_status_id")
    private Integer id;

    @NotNull
    @Column(name = "code")
    private String code;

    @NotNull
    @Column(name = "label")
    private String label;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }
}
