package com.db.bex.quiz.model;

import org.apache.commons.lang3.builder.HashCodeBuilder;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by calimih on 05/07/2016.
 */
@MappedSuperclass
public abstract class BaseEntity<PK extends Comparable> implements Serializable, Comparable<BaseEntity> {

    public abstract PK getId();

    public int compareTo(BaseEntity o) {
        return getId().compareTo(o.getId());
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof BaseEntity && getId().equals(((BaseEntity) obj).getId());
    }

    @Override
    public int hashCode() {
        if (getId() == null) {
            return 0;
        }
        return new HashCodeBuilder(7, 17).append(getId()).toHashCode();
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "date_created")
    private Date dateCreated;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "date_updated")
    private Date dateUpdated;

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Date getDateUpdated() {
        return dateUpdated;
    }

    public void setDateUpdated(Date dateUpdated) {
        this.dateUpdated = dateUpdated;
    }

}
