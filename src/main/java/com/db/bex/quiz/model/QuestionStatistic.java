package com.db.bex.quiz.model;

import javax.persistence.*;

/**
 * Created by calimih on 26/09/2016.
 */
@Entity
@Table(name = "tbl_question_statistic")
public class QuestionStatistic extends BaseEntity<Integer> {

    @Id
    @ManyToOne
    @JoinColumn(name = "question_id")
    private Question question;

    @Column(name = "correct_answers_pct")
    private Double correctAnswersPercentage;

    @Override
    public Integer getId() {
        return question.getId();
    }

    public Question getQuestion() {
        return question;
    }

    public void setQuestion(Question question) {
        this.question = question;
    }

    public Double getCorrectAnswersPercentage() {
        return correctAnswersPercentage;
    }

    public void setCorrectAnswersPercentage(Double correctAnswersPercentage) {
        this.correctAnswersPercentage = correctAnswersPercentage;
    }
}
