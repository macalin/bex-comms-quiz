package com.db.bex.quiz.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

/**
 * Created by calimih on 29/06/2016.
 */
@Entity
@Table(name="tbl_quiz")
public class Quiz extends AuditableEntity<Integer> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "quiz_id")
    private Integer id;

    @OneToOne
    @JoinColumn(name = "quiz_status_id")
    private QuizStatus status;

    @NotNull
    @Column(name = "name")
    private String name;

    @Column(name = "duration")
    private Integer duration;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "date_started")
    private Date dateStarted;

    @OneToMany
    @JoinColumn(name = "quiz_id")
    private List<Question> questions;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public QuizStatus getStatus() {
        return status;
    }

    public void setStatus(QuizStatus status) {
        this.status = status;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public Date getDateStarted() {
        return dateStarted;
    }

    public void setDateStarted(Date dateStarted) {
        this.dateStarted = dateStarted;
    }

    public List<Question> getQuestions() {
        return questions;
    }

    public void setQuestions(List<Question> questions) {
        this.questions = questions;
    }

}
