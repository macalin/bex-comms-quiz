package com.db.bex.quiz.model;


import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * Created by calimih on 29/06/2016.
 */
@Entity
@Table(name = "tbl_question_type")
public class QuestionType {

    public static final String SINGLE = "SINGLE";
    public static final String MULTI = "MULTI";
    public static final String TEXT = "TEXT";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "question_type_id")
    private Integer id;

    @NotNull
    @Column(name = "code")
    private String code;

    @NotNull
    @Column(name = "label")
    private String label;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }
}
