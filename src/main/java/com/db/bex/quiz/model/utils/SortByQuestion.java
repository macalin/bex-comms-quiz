package com.db.bex.quiz.model.utils;

import com.db.bex.quiz.model.UserAnswer;

import java.util.Comparator;

/**
 * Created by calimih on 18/07/2016.
 */
public class SortByQuestion implements Comparator<UserAnswer> {

    public int compare(UserAnswer o1, UserAnswer o2) {
        return o1.getQuestion().compareTo(o2.getQuestion());
    }
}
