package com.db.bex.quiz.model;

import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.validation.constraints.NotNull;

/**
 * Created by calimih on 05/07/2016.
 */
@MappedSuperclass
public abstract class AuditableEntity<PK extends Comparable> extends BaseEntity<PK> {

    @NotNull
    @ManyToOne
    @JoinColumn(name = "user_created")
    private User userCreated;

    @ManyToOne
    @JoinColumn(name = "user_updated")
    private User userUpdated;

    public User getUserCreated() {
        return userCreated;
    }

    public void setUserCreated(User userCreated) {
        this.userCreated = userCreated;
    }

    public User getUserUpdated() {
        return userUpdated;
    }

    public void setUserUpdated(User userUpdated) {
        this.userUpdated = userUpdated;
    }
}
