package com.db.bex.quiz.vo;

import org.apache.commons.lang3.builder.HashCodeBuilder;

/**
 * Created by calimih on 19/07/2016.
 */
public abstract class BaseVo<PK extends Comparable> implements Comparable<BaseVo>  {

    private PK id;

    public BaseVo() {
        super();
    }

    public BaseVo(PK id) {
        this.id = id;
    }

    public PK getId() {
        return id;
    }

    public void setId(PK id) {
        this.id = id;
    }

    public int compareTo(BaseVo o) {
        return id.compareTo(o.id);
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof BaseVo && id.equals(((BaseVo) obj).id);
    }

    @Override
    public int hashCode() {
        if (id == null) {
            return 0;
        }
        return new HashCodeBuilder(7, 17).append(id).toHashCode();
    }

}
