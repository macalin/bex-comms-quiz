package com.db.bex.quiz.vo;

import java.util.List;

/**
 * Created by calimih on 25/09/2016.
 */
public class QuizScoreVo {

    private String quizName;

    private List<UserScoreVo> userScores;

    public static class UserScoreVo {

        private String firstName;

        private String lastName;

        private Double score;

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        public Double getScore() {
            return score;
        }

        public void setScore(Double score) {
            this.score = score;
        }
    }

    public String getQuizName() {
        return quizName;
    }

    public void setQuizName(String quizName) {
        this.quizName = quizName;
    }

    public List<UserScoreVo> getUserScores() {
        return userScores;
    }

    public void setUserScores(List<UserScoreVo> userScores) {
        this.userScores = userScores;
    }
}

