package com.db.bex.quiz.vo;

import java.util.List;

/**
 * Created by calimih on 01/07/2016.
 */
public class QuestionVo extends BaseVo<Integer> {

    private String type;

    private String text;

    private List<AnswerVo> answers;

    private String answerText;

    private String correctText;

    public QuestionVo() {
        super();
    }

    public QuestionVo(Integer id) {
        super(id);
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public List<AnswerVo> getAnswers() {
        return answers;
    }

    public void setAnswers(List<AnswerVo> answers) {
        this.answers = answers;
    }

    public String getAnswerText() {
        return answerText;
    }

    public void setAnswerText(String answerText) {
        this.answerText = answerText;
    }

    public String getCorrectText() {
        return correctText;
    }

    public void setCorrectText(String correctText) {
        this.correctText = correctText;
    }
}
