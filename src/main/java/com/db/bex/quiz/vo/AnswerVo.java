package com.db.bex.quiz.vo;


/**
 * Created by calimih on 01/07/2016.
 */
public class AnswerVo extends BaseVo<Integer> {

    public AnswerVo() {
        super();
    }

    public AnswerVo(Integer id) {
        super(id);
    }

    private String text;

    private boolean selected;

    private boolean correct;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public boolean isCorrect() {
        return correct;
    }

    public void setCorrect(boolean correct) {
        this.correct = correct;
    }

}
