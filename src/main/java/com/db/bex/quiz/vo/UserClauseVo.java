package com.db.bex.quiz.vo;

import java.util.List;

public class UserClauseVo {
	
	private Boolean userExists;
	
	private List<String> clause;

	public Boolean getUserExists() {
		return userExists;
	}

	public void setUserExists(Boolean userExists) {
		this.userExists = userExists;
	}

	public List<String> getClause() {
		return clause;
	}

	public void setClause(List<String> clause) {
		this.clause = clause;
	}


}
