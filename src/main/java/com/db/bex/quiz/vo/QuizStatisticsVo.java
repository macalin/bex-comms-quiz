package com.db.bex.quiz.vo;

import java.util.List;

/**
 * Created by Andrei on 04/01/2017.
 */
public class QuizStatisticsVo {

    private String quizName;

    private List<QuestionStatisticVo> questionStatistics;

    public static class QuestionStatisticVo {

        private String question;

        private Double percentage;

        public String getQuestion() {
            return question;
        }

        public void setQuestion(String question) {
            this.question = question;
        }

        public Double getPercentage() {
            return percentage;
        }

        public void setPercentage(Double percentage) {
            this.percentage = percentage;
        }

    }

    public String getQuizName() {
        return quizName;
    }

    public void setQuizName(String quizName) {
        this.quizName = quizName;
    }

    public List<QuestionStatisticVo> getQuestionStatistics() {
        return questionStatistics;
    }

    public void setQuestionStatistics(List<QuestionStatisticVo> questionStatistics) {
        this.questionStatistics = questionStatistics;
    }
}
