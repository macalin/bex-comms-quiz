package com.db.bex.quiz.vo;

/**
 * Created by Andrei on 13/01/2017.
 */
public class AdministratorVo {

    private String username;

    private String name;

    private Boolean isCurrentUser;

    public Boolean getIsCurrentUser() {
        return isCurrentUser;
    }

    public void setIsCurrentUser(Boolean isCurrentUser) {
        this.isCurrentUser = isCurrentUser;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
