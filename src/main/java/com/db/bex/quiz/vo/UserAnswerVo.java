package com.db.bex.quiz.vo;

import java.util.List;

/**
 * Created by cergale on 08/07/2016.
 */

public class UserAnswerVo {

    private int questionId;

    private String answerText;

    private List<Integer> selectedAnswerIds;

    public int getQuestionId() {
        return questionId;
    }

    public void setQuestionId(int questionId) {
        this.questionId = questionId;
    }

    public String getAnswerText() {
        return answerText;
    }

    public void setAnswerText(String answerText) {
        this.answerText = answerText;
    }

    public List<Integer> getSelectedAnswerIds() {
        return selectedAnswerIds;
    }

    public void setSelectedAnswerIds(List<Integer> selectedAnswerIds) {
        this.selectedAnswerIds = selectedAnswerIds;
    }

}

