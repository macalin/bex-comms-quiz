package com.db.bex.quiz.m.controller;

import com.db.bex.quiz.m.service.AuthenticationService;
import com.db.bex.quiz.vo.UserClauseVo;
import io.jsonwebtoken.Claims;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by calimih on 30/06/2016.
 */
@Controller
public class LoginController {

    @Autowired
    private AuthenticationService authenticationService;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String showHomePage() {
        return "redirect:/index.html";
    }

    @RequestMapping(value = {"/login/user"}, method = RequestMethod.GET, produces = "text/plain")
    public ResponseEntity generateToken(@RequestParam String username, HttpServletRequest request) {
        authenticationService.generateToken(request, username);
        return new ResponseEntity<>(HttpStatus.OK);
    }
    
   	@RequestMapping(value = {"/login/user/clause"}, method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<UserClauseVo> userClause(@RequestParam String username, HttpServletRequest request) {
   		UserClauseVo userClause = authenticationService.getUserClause(username);
        return new ResponseEntity<>(userClause, HttpStatus.OK);
    }

    @RequestMapping(value = "/rest/login/user", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity saveUser(HttpServletRequest request) {
        Claims claims = (Claims)request.getAttribute("claims");
        authenticationService.saveUser(claims.getSubject());
        return new ResponseEntity(HttpStatus.OK);
    }
  
}
