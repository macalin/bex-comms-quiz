package com.db.bex.quiz.m.service.impl;

import com.db.bex.quiz.dao.UserDao;
import com.db.bex.quiz.dao.UserRoleDao;
import com.db.bex.quiz.m.service.AuthenticationService;
import com.db.bex.quiz.mail.MailService;
import com.db.bex.quiz.model.User;
import com.db.bex.quiz.model.UserRoleCode;
import com.db.bex.quiz.utils.HttpServerRequestUtils;
import com.db.bex.quiz.utils.TokenGenerator;
import com.db.bex.quiz.utils.UserUtils;
import com.db.bex.quiz.vo.UserClauseVo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by calimih on 01/07/2016.
 */
@Service(value = "authenticationService")
public class AuthenticationServiceImpl implements AuthenticationService {

    @Autowired
    private TokenGenerator tokenGenerator;

    @Autowired
    private Environment environment;

    @Autowired
    private UserRoleDao userRoleDao;

    @Autowired
    private UserDao userDao;

    private static final String NEW_LINE = System.getProperty("line.separator");
    
     @Autowired
    private MailService mailService;

    @Transactional(readOnly = false)
    public void generateToken(HttpServletRequest request, String username) {
        User user = userDao.findByUsername(username);
        boolean isAdmin = false;
        if (user != null && user.getUserRole() != null){
            isAdmin = UserRoleCode.ADMIN.getCode().equalsIgnoreCase(user.getUserRole().getCode());
        }
        String uri = HttpServerRequestUtils.getApplicationUri(request, isAdmin);
        String token = tokenGenerator.generateToken(username, isAdmin);
        // no need to save token and user  (it will be saved at first authentication)
        sendAuthenticationLink(username, token, uri);
    }
    
    @Transactional(readOnly = true)
    public UserClauseVo getUserClause(String username){
    	User user = userDao.findByUsername(username);
    	UserClauseVo userClause = new UserClauseVo();
    	boolean userExists = user != null;
    	if (!userExists) {
    		List<String> clause = new ArrayList<>();
    		clause.add(environment.getRequiredProperty("terms.and.conditions.line1"));
    		clause.add(environment.getRequiredProperty("terms.and.conditions.line2"));
    		clause.add(environment.getRequiredProperty("terms.and.conditions.line3"));
    		userClause.setClause(clause);
    	}
    	userClause.setUserExists(userExists);
    	
    	return userClause;
    }

    @Transactional(readOnly = false)
    public void saveUser(String username) {
        User user = userDao.findByUsername(username);
        if (user == null) {
            user = new User();
            // in the application we are able to create only common users
            // admin users are created at start time by running the populate-admins.sql script
            user.setUserRole(userRoleDao.findByCode(UserRoleCode.USER.getCode()));
            user.setUsername(username);
            user.setEmail(UserUtils.getEmail(username));
            user.setFirstName(UserUtils.getFirstName(username));
            user.setLastName(UserUtils.getLastName(username));
            // no need to set this for the moment
            //user.setDateUpdated(new Date());
            userDao.saveUser(user);
        }

    }

    private void sendAuthenticationLink(String username, String token, String uri) {
        String subject = environment.getRequiredProperty("mail.template.subject");
        StringBuilder message = new StringBuilder("Dear ")
                .append(UserUtils.getFirstName(username))
                .append(" ")
                .append(UserUtils.getLastName(username)).append(",").append(NEW_LINE).append(NEW_LINE)
                .append(environment.getRequiredProperty("mail.template.line1")).append(NEW_LINE).append(NEW_LINE)
                .append(environment.getRequiredProperty("terms.and.conditions.line1")).append(NEW_LINE).append(NEW_LINE)
                .append(environment.getRequiredProperty("terms.and.conditions.line2")).append(NEW_LINE).append(NEW_LINE)
                .append(environment.getRequiredProperty("terms.and.conditions.line3")).append(NEW_LINE).append(NEW_LINE)
                .append(environment.getRequiredProperty("mail.template.line2")).append(NEW_LINE)
                .append(uri)
                .append("token=").append(token).append(NEW_LINE).append(NEW_LINE);

        Boolean androidEnable = environment.getProperty("mail.template.android.enable", Boolean.class);
        if (androidEnable != null && androidEnable) {
            message.append(environment.getRequiredProperty("mail.template.android.line1")).append(NEW_LINE)
                    .append(uri.replace("http://", environment.getRequiredProperty("mail.template.android.name")))
                    .append("token=").append(token).append(NEW_LINE).append(NEW_LINE);
        }

        message.append(environment.resolveRequiredPlaceholders(environment.getRequiredProperty("mail.template.line3")))
            .append(NEW_LINE).append(NEW_LINE)
            .append(environment.getRequiredProperty("mail.template.line4")).append(NEW_LINE)
            .append(environment.getRequiredProperty("mail.template.line5"));
        mailService.sendMail(UserUtils.getEmail(username), subject, message.toString());
    }

}
