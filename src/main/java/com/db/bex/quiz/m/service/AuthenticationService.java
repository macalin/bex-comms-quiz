package com.db.bex.quiz.m.service;

import javax.servlet.http.HttpServletRequest;

import com.db.bex.quiz.vo.UserClauseVo;

/**
 * Created by calimih on 01/07/2016.
 */
public interface AuthenticationService {

    void generateToken(HttpServletRequest request, String email);
    
    UserClauseVo getUserClause(String email);

    void saveUser(String username);

}
