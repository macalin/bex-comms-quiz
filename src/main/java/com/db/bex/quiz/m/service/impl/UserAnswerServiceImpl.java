package com.db.bex.quiz.m.service.impl;

import com.db.bex.quiz.dao.*;
import com.db.bex.quiz.m.service.UserAnswerService;
import com.db.bex.quiz.model.Question;
import com.db.bex.quiz.model.QuizStatus;
import com.db.bex.quiz.model.UserAnswer;
import com.db.bex.quiz.model.UserQuiz;
import com.db.bex.quiz.security.exception.UnauthorizedException;
import com.db.bex.quiz.vo.UserAnswerVo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by calimih on 06/07/2016.
 */
@Service
@Transactional
public class UserAnswerServiceImpl implements UserAnswerService {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserAnswerServiceImpl.class);

    @Autowired
    QuizDao quizDao;

    @Autowired
    UserDao userDao;

    @Autowired
    QuestionDao questionDao;

    @Autowired
    AnswerDao answerDao;

    @Autowired
    UserQuizDao userQuizDao;

    @Autowired
    UserAnswerDao userAnswerDao;

    public void saveUserAnswer(String username, UserAnswerVo userAnswerVo) {
        Question question = questionDao.findById(userAnswerVo.getQuestionId());
        if (!QuizStatus.ACTIVE.equals(question.getQuiz().getStatus().getCode())) {
            String errorMessage = "Hack Attempt: User " + username + " tried to save question for an inactive quiz!";
            LOGGER.warn(errorMessage);
            throw new UnauthorizedException(errorMessage);
        }
        userAnswerDao.deleteByUsernameAndQuestionId(username, userAnswerVo.getQuestionId());
        if (userAnswerVo.getSelectedAnswerIds() != null) {
            for (Integer answerId : userAnswerVo.getSelectedAnswerIds()) {
                userAnswerDao.saveUserAnswer(getUserAnswerForSave(username, userAnswerVo.getQuestionId(), answerId));
            }
        } else if (userAnswerVo.getAnswerText() != null) {
            userAnswerDao.saveUserAnswer(getUserAnswerTextForSave(username, userAnswerVo.getQuestionId(), userAnswerVo.getAnswerText()));
        }
    }

    private UserAnswer getUserAnswerForSave(String username, Integer questionId, Integer answerId) {
        UserAnswer userAnswer = new UserAnswer();
        Question question = questionDao.findById(questionId);
        userAnswer.setQuestion(question);
        userAnswer.setAnswer(answerDao.findById(answerId));
        userAnswer.setUserQuiz(getUserQuiz(username, question.getQuiz().getId()));
        return userAnswer;
    }

    private UserAnswer getUserAnswerTextForSave(String username, Integer questionId, String answerText) {
        UserAnswer userAnswer = new UserAnswer();
        Question question = questionDao.findById(questionId);
        userAnswer.setQuestion(question);
        userAnswer.setAnswerText(answerText);
        userAnswer.setUserQuiz(getUserQuiz(username, question.getQuiz().getId()));
        return userAnswer;
    }

    private UserQuiz getUserQuiz(String username, Integer quizId) {
        UserQuiz userQuiz = userQuizDao.findByUsernameAndQuizId(username, quizId);
        if (userQuiz == null) {
            userQuiz = new UserQuiz();
            userQuiz.setQuiz(quizDao.findById(quizId));
            userQuiz.setUser(userDao.findByUsername(username));
            userQuizDao.saveUserQuiz(userQuiz);
        }
        return userQuiz;
    }

}
