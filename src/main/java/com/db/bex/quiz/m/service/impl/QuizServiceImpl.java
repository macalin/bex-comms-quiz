package com.db.bex.quiz.m.service.impl;

import com.db.bex.quiz.dao.QuizDao;
import com.db.bex.quiz.dao.UserAnswerDao;
import com.db.bex.quiz.dao.UserQuizDao;
import com.db.bex.quiz.m.service.QuizService;
import com.db.bex.quiz.model.*;
import com.db.bex.quiz.security.exception.UnauthorizedException;
import com.db.bex.quiz.vo.AnswerVo;
import com.db.bex.quiz.vo.QuestionVo;
import com.db.bex.quiz.vo.QuizVo;
import com.db.bex.quiz.web.service.ScoreService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import static com.db.bex.quiz.model.QuestionType.*;

/**
 * Created by calimih on 29/06/2016.
 */
@Service("quizService")
@Transactional
public class QuizServiceImpl implements QuizService {

    private static final Logger LOGGER = LoggerFactory.getLogger(QuizServiceImpl.class);

    private final Long SUBMIT_LAG_FIX_SECONDS = 5L;

    @Autowired
    QuizDao quizDao;

    @Autowired
    UserAnswerDao userAnswerDao;

    @Autowired
    UserQuizDao userQuizDao;
    
    @Autowired
    ScoreService scoreService;

    public List<QuizVo> findUserQuizzes(String username) {
        List<QuizVo> userQuizzes = quizDao.findActiveQuizzes(SUBMIT_LAG_FIX_SECONDS).stream()
                .map(quiz -> convertModelToVo(quiz))
                .collect(Collectors.toList());
        userQuizzes.addAll(userQuizDao.findUserCompletedQuizzes(username).stream()
                .map(userQuiz -> convertUserQuizToVo(userQuiz))
                .collect(Collectors.toList()));
        return userQuizzes;
    }

    public QuizVo findActiveQuiz(String username, int id) {
        Quiz quiz = quizDao.findById(id);
        if (!QuizStatus.ACTIVE.equals(quiz.getStatus().getCode())) {
            String errorMessage = "Hack Attempt: User " + username + " tried to see an inactive quiz!";
            LOGGER.warn(errorMessage);
            throw new UnauthorizedException(errorMessage);
        }
        return convertQuizToVo(username, quiz, false);
    }

    public QuizVo findCompletedQuizAnswers(String username, int id) {
        QuizVo quizVo = new QuizVo();
        quizVo.setUserName(username);
        UserQuiz userQuiz = userQuizDao.findByUsernameAndQuizId(username, id);
        if (!QuizStatus.COMPLETED.equals(userQuiz.getQuiz().getStatus().getCode())) {
            String errorMessage = "Hack Attempt: User " + username + " tried to see the answers for a quiz that was not completed!";
            LOGGER.warn(errorMessage);
            throw new UnauthorizedException(errorMessage);
        }
        return convertQuizToVo(username, userQuiz);
    }

    private QuizVo convertQuizToVo(String username, UserQuiz userQuiz) {
        Quiz quiz = userQuiz.getQuiz();
        QuizVo quizVo = convertQuizToVo(username, quiz, true);
        return quizVo;
    }

    private QuizVo convertQuizToVo(String username, Quiz quiz, Boolean showCorrectAnswers) {
        QuizVo quizVo = new QuizVo();
        quizVo.setUserName(username);
        quizVo.setId(quiz.getId());
        quizVo.setName(quiz.getName());
        quizVo.setQuestions(quiz.getQuestions().stream()
                .map(question -> convertQuestionToVo(username, question, showCorrectAnswers))
                .collect(Collectors.toList()));
        quizVo.setRemainingSeconds(getUserRemainingSeconds(quiz));
        return quizVo;
    }

    private long getUserRemainingSeconds(Quiz quiz) {
        Long passedTime = TimeUnit.MILLISECONDS.toSeconds((new Date()).getTime() - quiz.getDateStarted().getTime());
        Long remainingTime = TimeUnit.MINUTES.toSeconds(quiz.getDuration()) - passedTime - SUBMIT_LAG_FIX_SECONDS;
        return remainingTime;
    }

    private QuestionVo convertQuestionToVo(String username, Question question, Boolean showCorrectAnswers) {
        QuestionVo questionVo = new QuestionVo();
        String questionType = question.getType().getCode();
        questionVo.setId(question.getId());
        questionVo.setType(questionType.toLowerCase());
        questionVo.setText(question.getText());
        List<UserAnswer> userAnswers = userAnswerDao.findByUsernameAndQuestionId(username, question.getId());
        questionVo.setAnswers(question.getAnswers().stream()
                .map(answer -> convertAnswerToVo(answer, userAnswers, questionType, showCorrectAnswers))
                .collect(Collectors.toList()));
        if (TEXT.equals(questionType)) {
        	populateTextAnswer(questionVo, question, userAnswers, showCorrectAnswers);
        }
        return questionVo;
    }
    
    private void populateTextAnswer(QuestionVo questionVo, Question question, List<UserAnswer> userAnswers, Boolean showCorrectAnswers) {
    	String userAnswerText = StringUtils.EMPTY;
    	if (userAnswers != null && userAnswers.size() > 0) {
    		userAnswerText = userAnswers.get(0).getAnswerText();
    	}
    	if (showCorrectAnswers) {
    		String correctText = question.getCorrectAnswers().get(0).getText();
    		questionVo.setCorrectText(correctText);
    		if (!scoreService.isSameTextAnswer(correctText, userAnswerText)){
    			questionVo.setAnswerText(userAnswerText);
    		}
    	} else {
    		questionVo.setAnswerText(userAnswerText);
    	}
    }

    private AnswerVo convertAnswerToVo(Answer answer, List<UserAnswer> userAnswers, String questionType, Boolean showCorrectAnswers) {
        AnswerVo answerVo = new AnswerVo();
        answerVo.setId(answer.getId());
        answerVo.setText(answer.getText());
        answerVo.setSelected(userAnswersContainsAnswer(userAnswers, answer));
        if (showCorrectAnswers && (SINGLE.equals(questionType) || MULTI.equals(questionType))) {
            answerVo.setCorrect(answer.getCorrectAnswer() != null);
        }
       return answerVo;
    }

    private boolean userAnswersContainsAnswer(List<UserAnswer> userAnswers, Answer answer) {
        for (UserAnswer userAnswer : userAnswers) {
            if (userAnswer != null && userAnswer.getAnswer().equals(answer)) {
                return true;
            }
        }
        return false;
    }

    private QuizVo convertUserQuizToVo(UserQuiz userQuiz) {
        Quiz quiz = userQuiz.getQuiz();
        QuizVo vo = convertModelToVo(quiz);
        String userScore = userQuiz.getScore() != null ? userQuiz.getScore().toString() : "";
        vo.setScore(userScore + "/" + quiz.getQuestions().size());
        return vo;
    }

    private QuizVo convertModelToVo(Quiz model) {
        QuizVo vo = new QuizVo();
        vo.setId(model.getId());
        vo.setName(model.getName());
        vo.setStatus(model.getStatus().getCode());
        if (QuizStatus.ACTIVE.equals(model.getStatus().getCode())) {
            vo.setRemainingSeconds(getUserRemainingSeconds(model));
        }
        return vo;
    }

}
