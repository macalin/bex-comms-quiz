package com.db.bex.quiz.m.controller;

import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Andrei on 14/02/2017.
 */
@RestController
public class QuizNotifierController {

    @MessageMapping("/new")
    @SendTo("/topic/quiz")
    public Boolean notifyNewQuiz(){
        return Boolean.TRUE;
    }
}
