package com.db.bex.quiz.m.service;

import com.db.bex.quiz.vo.QuizVo;

import java.util.List;

/**
 * Service for User quiz
 * Created by calimih on 29/06/2016.
 */
public interface QuizService {

    /**
     * finds all active quizzes
     * @return
     * @param username
     */
    List<QuizVo> findUserQuizzes(String username);

    /**
     * find quiz by id
     * @param id
     * @return
     */
    QuizVo findActiveQuiz(String username, int id);

    /**
     * find user quiz answers by username and quiz id
     * @param id
     * @return
     */
    QuizVo findCompletedQuizAnswers(String username, int id);

}
