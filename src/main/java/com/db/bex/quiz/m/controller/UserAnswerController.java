package com.db.bex.quiz.m.controller;

import com.db.bex.quiz.m.service.UserAnswerService;
import com.db.bex.quiz.vo.UserAnswerVo;
import io.jsonwebtoken.Claims;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by calimih on 06/07/2016.
 */
@Controller
@RequestMapping(value = "/rest/quiz/user")
public class UserAnswerController {

    @Autowired
    UserAnswerService userAnswerService;

    @RequestMapping(value = "/answer", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    public ResponseEntity saveUserAnswer(HttpServletRequest request, @RequestBody UserAnswerVo userAnswerVo) {
        Claims claims = (Claims)request.getAttribute("claims");
        userAnswerService.saveUserAnswer(claims.getSubject(), userAnswerVo);
        return new ResponseEntity(HttpStatus.OK);
    }

}
