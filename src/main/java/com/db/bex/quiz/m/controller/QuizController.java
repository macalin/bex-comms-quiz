package com.db.bex.quiz.m.controller;

import com.db.bex.quiz.m.service.QuizService;
import com.db.bex.quiz.vo.QuizVo;
import io.jsonwebtoken.Claims;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Controller for User Quiz related requests
 * Created by calimih on 29/06/2016.
 */
@Controller
@RequestMapping("/rest/quiz")
public class QuizController {

    @Autowired
    QuizService quizService;

    /**
     * Gets quiz with all questions, answers and user answers
     * @param request
     * @param quizId
     * @return
     */
    @RequestMapping(method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<QuizVo> getQuiz(HttpServletRequest request, @RequestParam int quizId) {
        Claims claims = (Claims)request.getAttribute("claims");
        QuizVo quiz = quizService.findActiveQuiz(claims.getSubject(),quizId);
        return new ResponseEntity<>(quiz, HttpStatus.OK);
    }

    /**
     * Gets quiz with all questions, answers, correct answers and user answers
     * @param request
     * @param quizId
     * @return
     */
    @RequestMapping(value = "/answers", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<QuizVo> getQuizAnswers(HttpServletRequest request, @RequestParam int quizId) {
        Claims claims = (Claims)request.getAttribute("claims");
        QuizVo quiz = quizService.findCompletedQuizAnswers(claims.getSubject(),quizId);
        return new ResponseEntity<>(quiz, HttpStatus.OK);
    }

    @RequestMapping(value = "/active", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<List<QuizVo>> getActiveQuizzes(HttpServletRequest request) {
        Claims claims = (Claims)request.getAttribute("claims");
        List<QuizVo> quizzes = quizService.findUserQuizzes(claims.getSubject());
        return new ResponseEntity<>(quizzes, HttpStatus.OK);
    }

}
