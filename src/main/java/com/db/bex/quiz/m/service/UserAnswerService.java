package com.db.bex.quiz.m.service;

import com.db.bex.quiz.vo.UserAnswerVo;

/**
 * Service for user answer
 * Created by calimih on 06/07/2016.
 */
public interface UserAnswerService {

    /**
     * Saves the user answer for a question
     * @param username
     * @param userAnswerVo
     */
    void saveUserAnswer(String username, UserAnswerVo userAnswerVo);

}
