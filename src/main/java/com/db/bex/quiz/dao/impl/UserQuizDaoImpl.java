package com.db.bex.quiz.dao.impl;

import com.db.bex.quiz.dao.AbstractDao;
import com.db.bex.quiz.dao.UserQuizDao;
import com.db.bex.quiz.model.QuizStatus;
import com.db.bex.quiz.model.UserQuiz;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by calimih on 13/07/2016.
 */
@Repository
public class UserQuizDaoImpl extends AbstractDao<Integer, UserQuiz> implements UserQuizDao {

    private final static String GET_USER_COMPLETED_QUIZZES = "select usrq.* from tbl_user_quiz usrq " +
            " inner join tbl_quiz qz on usrq.quiz_id = qz.quiz_id where"
            + " usrq.user_id = :username"
            + " and qz.quiz_status_id = (select quiz_status_id from tbl_quiz_status qzst where qzst.code = :quizStatus)"
            + " and qz.duration * 60 < (UNIX_TIMESTAMP(STR_TO_DATE(:newDate, '%Y-%m-%d %H:%i:%s')) - UNIX_TIMESTAMP(qz.date_started))"
            + " order by qz.date_started desc";

    public List<UserQuiz> findByQuizId(Integer quizId) {
        if (quizId == null) throw new IllegalArgumentException();
        List<UserQuiz> userQuiz = query("from UserQuiz usrq where usrq.quiz.id = :quizId " +
                "order by usrq.score desc, usrq.user.firstName, usrq.user.lastName")
                .setParameter("quizId", quizId)
                .list();
        return userQuiz;
    }

    public UserQuiz findByUsernameAndQuizId(String username, Integer quizId) {
        if (username == null) throw new IllegalArgumentException();
        if (quizId == null) throw new IllegalArgumentException();

        UserQuiz userQuiz = (UserQuiz) query("from UserQuiz usrq where usrq.user.username = :username and usrq.quiz.id = :quizId")
                .setParameter("username", username)
                .setParameter("quizId", quizId)
                .uniqueResult();
        return userQuiz;
    }

    public void saveUserQuiz(UserQuiz userQuiz) {
        saveOrUpdate(userQuiz);
    };

    public List<UserQuiz> findHighScorers(Integer quizId) {
        if (quizId == null) throw new IllegalArgumentException();
        List<UserQuiz> users = query("from UserQuiz usrq where usrq.quiz.id = :quizId and score = (select max(score) from UserQuiz usrq where usrq.quiz.id = :quizId) " +
                "order by usrq.user.firstName, usrq.user.lastName")
                .setParameter("quizId", quizId)
                .list();
        return users;
    }

    public List<UserQuiz> findUserCompletedQuizzes(String username) {
        if (username == null) throw new IllegalArgumentException();
        Date currentDate = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Query query = createSQLQuery(GET_USER_COMPLETED_QUIZZES).addEntity(UserQuiz.class)
                .setParameter("username", username)
                .setParameter("quizStatus", QuizStatus.COMPLETED)
                .setParameter("newDate", dateFormat.format(currentDate));
        return query.list();
    }

}
