package com.db.bex.quiz.dao.impl;

import com.db.bex.quiz.dao.AbstractDao;
import com.db.bex.quiz.dao.UserAnswerDao;
import com.db.bex.quiz.model.UserAnswer;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by calimih on 06/07/2016.
 */
@Repository
public class UserAnswerDaoImpl extends AbstractDao<Integer, UserAnswer> implements UserAnswerDao {

    public void deleteByUsernameAndQuestionId(String username, Integer questionId) {
        if (username == null) throw new IllegalArgumentException();
        if (questionId == null) throw new IllegalArgumentException();
        List<UserAnswer> userAnswers = findByUsernameAndQuestionId(username, questionId);
        for (UserAnswer userAnswer: userAnswers) {
            delete(userAnswer);
        }
    }

    public List<UserAnswer> findByUsernameAndQuestionId(String username, Integer questionId) {
        if (username == null) throw new IllegalArgumentException();
        if (questionId == null) throw new IllegalArgumentException();
        List<UserAnswer> userAnswers = query("from UserAnswer usra where usra.userQuiz.user.username = :username and usra.question.id = :questionId")
                .setParameter("username", username)
                .setParameter("questionId", questionId)
                .list();
        return userAnswers;
    }

    public void saveUserAnswer(UserAnswer userAnswer) {
        saveOrUpdate(userAnswer);
    }

}
