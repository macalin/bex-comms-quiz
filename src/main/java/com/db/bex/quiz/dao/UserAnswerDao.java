package com.db.bex.quiz.dao;

import com.db.bex.quiz.model.UserAnswer;

import java.util.List;

/**
 * Dao for tbl_user_answer
 * Created by calimih on 06/07/2016.
 */
public interface UserAnswerDao {

    /**
     * Deletes all user answers for given username and question id
     * @param username
     * @param questionId
     */
    void deleteByUsernameAndQuestionId(String username, Integer questionId);

    /**
     * Finds all user answers for given username and question id
     * @param username
     * @param questionId
     * @return
     */
    public List<UserAnswer> findByUsernameAndQuestionId(String username, Integer questionId);

    /**
     * Saves user answer
     * @param userAnswer
     */
    void saveUserAnswer(UserAnswer userAnswer);

}
