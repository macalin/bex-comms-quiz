package com.db.bex.quiz.dao.impl;

import com.db.bex.quiz.dao.AbstractDao;
import com.db.bex.quiz.dao.QuizStatusDao;
import com.db.bex.quiz.model.QuizStatus;
import org.springframework.stereotype.Repository;

/**
 * Created by calimih on 19/07/2016.
 */
@Repository
public class QuizStatusDaoImpl extends AbstractDao<Integer, QuizStatus> implements QuizStatusDao {

    public QuizStatus findByCode(String quizStatusCode) {
        if (quizStatusCode == null) throw new IllegalArgumentException();
        QuizStatus quizStatus = (QuizStatus) query("from QuizStatus qzst where qzst.code = :quizStatusCode")
                .setParameter("quizStatusCode", quizStatusCode)
                .uniqueResult();
        return quizStatus;
    }
}
