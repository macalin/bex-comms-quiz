package com.db.bex.quiz.dao.impl;

import com.db.bex.quiz.dao.AbstractDao;
import com.db.bex.quiz.dao.UserRoleDao;
import com.db.bex.quiz.model.UserRole;
import org.springframework.stereotype.Repository;

/**
 * Created by calimih on 06/07/2016.
 */
@Repository
public class UserRoleDaoImpl extends AbstractDao<Integer, UserRole> implements UserRoleDao {

    public UserRole findByCode(String code) {
        if (code == null) throw new IllegalArgumentException();
        UserRole userRole = (UserRole) query("from UserRole usrr where usrr.code=:code")
                .setParameter("code", code)
                .uniqueResult();
        return userRole;
    }
}
