package com.db.bex.quiz.dao;

import com.db.bex.quiz.model.Answer;

import java.util.List;

/**
 * Dao for tbl_answer table
 * Created by calimih on 13/07/2016.
 */
public interface AnswerDao {

    /**
     * Finds an answer by id
     * @param id - answer_id
     * @return   - an answer
     */
    Answer findById(int id);

    /**
     * Finds all answers associated with a question
     * @param questionId
     * @return
     */
    List<Answer> findByQuestionId(Integer questionId);

    /**
     * Deletes an answer
     * @param answer
     */
    void deleteAnswer(Answer answer);

    /**
     * Deletes all given answers
     * @param answers
     */
    void deleteAnswers(List<Answer> answers);

    /**
     * Saves given answer
     * @param answer
     */
    void saveAnswer(Answer answer);

}
