package com.db.bex.quiz.dao.impl;

import com.db.bex.quiz.dao.AbstractDao;
import com.db.bex.quiz.dao.QuestionDao;
import com.db.bex.quiz.model.Question;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by calimih on 13/07/2016.
 */
@Repository
public class QuestionDaoImpl extends AbstractDao<Integer, Question> implements QuestionDao {

    public Question findById(int id) {
        return getByKey(id);
    }

    public List<Question> findByQuizId(Integer quizId) {
        if (quizId == null) throw new IllegalArgumentException();
        List<Question> questionList = query("from Question qstn where qstn.quiz.id = :quizId")
                .setParameter("quizId", quizId)
                .list();
        return questionList;
    }

    public void deleteQuestions(List<Question> questions) {
        for (Question question : questions) {
            delete(question);
        }
    }

    public void deleteQuestion(Question question) {
        delete(question);
    }

    public void saveQuestion(Question question) {
        saveOrUpdate(question);
    }

}
