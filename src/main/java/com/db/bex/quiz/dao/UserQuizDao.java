package com.db.bex.quiz.dao;

import com.db.bex.quiz.model.UserQuiz;

import java.util.List;

/**
 * Dao for tbl_user_quiz table
 * Created by calimih on 13/07/2016.
 */
public interface UserQuizDao {

    /**
     * Find all users answers for a given quiz
     * @param quizId
     * @return
     */
    List<UserQuiz> findByQuizId(Integer quizId);

    /**
     * Finds an user quiz by username and quiz id
     * @param username
     * @param quizId
     * @return
     */
    UserQuiz findByUsernameAndQuizId(String username, Integer quizId);

    /**
     * Saves userQuiz
     * @param userQuiz
     */
    void saveUserQuiz(UserQuiz userQuiz);

    /**
     * Finds the users with the high sore for the given quiz
     * @param quizId
     * @return
     */
    List<UserQuiz> findHighScorers(Integer quizId);

    /**
     * Find all completed users quizzes
     * @param username
     * @return
     */
    List<UserQuiz> findUserCompletedQuizzes(String username);

}
