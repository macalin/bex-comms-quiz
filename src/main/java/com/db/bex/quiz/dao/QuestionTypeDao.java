package com.db.bex.quiz.dao;

import com.db.bex.quiz.model.QuestionType;

/**
 * Created by calimih on 19/07/2016.
 */
public interface QuestionTypeDao {

    QuestionType findByCode(String code);

}
