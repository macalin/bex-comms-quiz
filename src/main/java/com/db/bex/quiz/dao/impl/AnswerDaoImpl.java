package com.db.bex.quiz.dao.impl;

import com.db.bex.quiz.dao.AbstractDao;
import com.db.bex.quiz.dao.AnswerDao;
import com.db.bex.quiz.model.Answer;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by calimih on 13/07/2016.
 */
@Repository
public class AnswerDaoImpl extends AbstractDao<Integer, Answer> implements AnswerDao {

    public Answer findById(int id) {
        return getByKey(id);
    }

    public List<Answer> findByQuestionId(Integer questionId) {
        if (questionId == null) throw new IllegalArgumentException();
        List<Answer> answerList = query("from Answer a where a.question.id = :questionId")
                .setParameter("questionId", questionId)
                .list();
        return answerList;
    }

    public void deleteAnswer(Answer answer) {
        delete(answer);
    }

    public void deleteAnswers(List<Answer> answers) {
        for (Answer answer : answers) {
            delete(answer);
        }
    }

    public void saveAnswer(Answer answer) {
        saveOrUpdate(answer);
    }
}
