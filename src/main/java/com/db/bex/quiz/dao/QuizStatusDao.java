package com.db.bex.quiz.dao;

import com.db.bex.quiz.model.QuizStatus;

/**
 * Created by calimih on 19/07/2016.
 */
public interface QuizStatusDao {

    QuizStatus findByCode(String code);

}
