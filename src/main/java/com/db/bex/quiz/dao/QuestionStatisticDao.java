package com.db.bex.quiz.dao;

import com.db.bex.quiz.model.QuestionStatistic;

import java.util.List;

/**
 * Created by calimih on 26/09/2016.
 */
public interface QuestionStatisticDao {

    List<QuestionStatistic> findByQuizId(Integer quizId);

    /**
     * Saves the given question statistic
     * @param questionStatistic
     */
    void saveQuestionStatistic(QuestionStatistic questionStatistic);

}
