package com.db.bex.quiz.dao;

import com.db.bex.quiz.model.Quiz;

import java.util.List;

/**
 * Created by calimih on 29/06/2016.
 */
public interface QuizDao {

    /**
     * Finds a quiz by id
     * @param id
     * @return
     */
    Quiz findById(int id);

    /**
     * Finds all quizzes by given status
     * @param quizStatus
     * @return
     */
    List<Quiz> findQuizzesByStatus(String quizStatus);

    /**
     * Finds all Active Quizzes
     * @return
     * @param submitLagFixSeconds
     */
    List<Quiz> findActiveQuizzes(Long submitLagFixSeconds);

    /**
     * Saves the given quiz
     * @param quiz
     */
    void saveQuiz(Quiz quiz);

    /**
     * Deletes the given quiz
     * @param quiz
     */
    void deleteQuiz(Quiz quiz);


}
