package com.db.bex.quiz.dao.impl;

import com.db.bex.quiz.dao.AbstractDao;
import com.db.bex.quiz.dao.CorrectAnswerDao;
import com.db.bex.quiz.model.CorrectAnswer;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by calimih on 19/07/2016.
 */
@Repository
public class CorrectAnswerDaoImpl extends AbstractDao<Integer, CorrectAnswer> implements CorrectAnswerDao {

    public CorrectAnswer findByQuestionId(Integer questionId) {
        if (questionId == null) throw new IllegalArgumentException();
        CorrectAnswer answer = (CorrectAnswer) query("from CorrectAnswer casr where casr.question.id = :questionId")
                .setParameter("questionId", questionId)
                .uniqueResult();
        return answer;
    }

    public CorrectAnswer findByQuestionIdAndAnswerId(Integer questionId, Integer answerId) {
        if (questionId == null) throw new IllegalArgumentException();
        if (answerId == null) throw new IllegalArgumentException();
        CorrectAnswer answer = (CorrectAnswer) query("from CorrectAnswer casr where casr.question.id = :questionId and casr.answer.id = :answerId")
                .setParameter("questionId", questionId)
                .setParameter("answerId", answerId)
                .uniqueResult();
        return answer;
    }

    public void deleteCorrectAnswer(CorrectAnswer correctAnswer) {
        delete(correctAnswer);
    }

    public void deleteCorrectAnswers(List<CorrectAnswer> correctAnswersList) {
        for (CorrectAnswer correctAnswer : correctAnswersList) {
            delete(correctAnswer);
        }
    }

    public void saveCorrectAnswer(CorrectAnswer correctAnswer) {
        saveOrUpdate(correctAnswer);
    }
}
