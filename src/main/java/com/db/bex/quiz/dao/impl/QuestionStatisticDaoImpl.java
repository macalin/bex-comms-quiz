package com.db.bex.quiz.dao.impl;

import com.db.bex.quiz.dao.AbstractDao;
import com.db.bex.quiz.dao.QuestionStatisticDao;
import com.db.bex.quiz.model.QuestionStatistic;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by calimih on 26/09/2016.
 */
@Repository
public class QuestionStatisticDaoImpl extends AbstractDao<Integer, QuestionStatistic> implements QuestionStatisticDao {

    public List<QuestionStatistic> findByQuizId(Integer quizId) {
        if (quizId == null) throw new IllegalArgumentException();
        List<QuestionStatistic> questionStatistics = query("from QuestionStatistic qnst where qnst.question.quiz.id = :quizId")
                .setParameter("quizId", quizId)
                .list();
        return questionStatistics;
    }

    public void saveQuestionStatistic(QuestionStatistic questionStatistic) {
        saveOrUpdate(questionStatistic);
    }

}
