package com.db.bex.quiz.dao;

import com.db.bex.quiz.model.Question;

import java.util.List;

/**
 * Dao for tbl_question table
 * Created by calimih on 13/07/2016.
 */
public interface QuestionDao {

    /**
     * Finds a question by id
     * @param id - question_id
     * @return   - a question
     */
    Question findById(int id);

    /**
     * Finds the questions for a given Quiz id
     * @param quizId
     * @return
     */
    List<Question> findByQuizId(Integer quizId);

    /**
     * Deletes the given question
     * @param question
     */
    void deleteQuestion(Question question);

    /**
     * Deletes the given questions
     * @param questions
     */
    void deleteQuestions(List<Question> questions);

    /**
     * Saves the given question
     * @param question
     */
    void saveQuestion(Question question);

}
