package com.db.bex.quiz.dao;

import com.db.bex.quiz.model.User;

import java.util.List;

/**
 * Created by calimih on 04/07/2016.
 */
public interface UserDao {

    User findByUsername(String username);

    void saveUser(User user);

    List<User> loadAdministrators();

}
