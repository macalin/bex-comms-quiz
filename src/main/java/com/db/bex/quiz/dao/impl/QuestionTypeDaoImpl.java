package com.db.bex.quiz.dao.impl;

import com.db.bex.quiz.dao.AbstractDao;
import com.db.bex.quiz.dao.QuestionTypeDao;
import com.db.bex.quiz.model.QuestionType;
import org.springframework.stereotype.Repository;

/**
 * Created by calimih on 19/07/2016.
 */
@Repository
public class QuestionTypeDaoImpl extends AbstractDao<Integer, QuestionType> implements QuestionTypeDao {

    public QuestionType findByCode(String questionTypeCode) {
        if (questionTypeCode == null) throw new IllegalArgumentException();
        QuestionType questionType = (QuestionType) query("from QuestionType qstt where qstt.code = :questionTypeCode")
                .setParameter("questionTypeCode", questionTypeCode)
                .uniqueResult();
        return questionType;
    }

}
