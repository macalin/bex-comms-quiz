package com.db.bex.quiz.dao.impl;

import com.db.bex.quiz.dao.AbstractDao;
import com.db.bex.quiz.dao.QuizDao;
import com.db.bex.quiz.model.Quiz;
import com.db.bex.quiz.model.QuizStatus;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by calimih on 29/06/2016.
 */
@Repository
public class QuizDaoImpl extends AbstractDao<Integer, Quiz> implements QuizDao {

    private final static String GET_ACTIVE_QUIZZES = "select * from tbl_quiz qz where"
            + " qz.quiz_status_id = (select quiz_status_id from tbl_quiz_status qzst where qzst.code = :quizStatus)"
            + " and qz.duration * 60 - :submitLagFixSeconds > (UNIX_TIMESTAMP(STR_TO_DATE(:newDate, '%Y-%m-%d %H:%i:%s')) - UNIX_TIMESTAMP(qz.date_started))"
            + " order by qz.date_started";

    public Quiz findById(int id) {
        return getByKey(id);
    }

    public List<Quiz> findQuizzesByStatus(String quizStatus) {
        return query("from Quiz quiz where quiz.status.code = :quizStatus")
                .setParameter("quizStatus", quizStatus)
                .list();
    }

    public List<Quiz> findActiveQuizzes(Long submitLagFixSeconds) {
        Date currentDate = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Query query = createSQLQuery(GET_ACTIVE_QUIZZES).addEntity(Quiz.class)
                .setParameter("quizStatus", QuizStatus.ACTIVE)
                .setParameter("newDate", dateFormat.format(currentDate))
                .setParameter("submitLagFixSeconds", submitLagFixSeconds);
        return query.list();
    }

    public void saveQuiz(Quiz quiz) {
        persist(quiz);
    }

    public void deleteQuiz(Quiz quiz) {
        delete(quiz);
    }
}