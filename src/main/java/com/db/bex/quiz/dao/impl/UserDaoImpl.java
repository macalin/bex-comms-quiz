package com.db.bex.quiz.dao.impl;

import com.db.bex.quiz.dao.AbstractDao;
import com.db.bex.quiz.dao.UserDao;
import com.db.bex.quiz.model.User;
import com.db.bex.quiz.model.UserRoleCode;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by calimih on 04/07/2016.
 */
@Repository
public class UserDaoImpl extends AbstractDao<String, User> implements UserDao {

    public User findByUsername(String username) {
        return getByKey(username);
    }

    public void saveUser(User user) {
        persist(user);
    }

    @Override
    public List<User> loadAdministrators() {
        List<User> administrators = query("from User usr where usr.userRole.code = :adminCode")
                .setParameter("adminCode", UserRoleCode.ADMIN.getCode())
                .list();
        return administrators;
    }

}
