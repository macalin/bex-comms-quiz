package com.db.bex.quiz.dao;

import com.db.bex.quiz.model.UserRole;

/**
 * Created by calimih on 06/07/2016.
 */
public interface UserRoleDao {

    UserRole findByCode(String code);

}
