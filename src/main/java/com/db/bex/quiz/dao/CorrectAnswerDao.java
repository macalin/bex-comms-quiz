package com.db.bex.quiz.dao;

import com.db.bex.quiz.model.CorrectAnswer;

import java.util.List;

/**
 * Created by calimih on 19/07/2016.
 */
public interface CorrectAnswerDao {

    /**
     * Finds a correct answer by question id (used for question of type text)
     * @param questionId
     * @return
     */
    CorrectAnswer findByQuestionId(Integer questionId);

    /**
     * Finds if an answer is correct answer
     * @param questionId
     * @param answerId
     * @return
     */
    CorrectAnswer findByQuestionIdAndAnswerId(Integer questionId, Integer answerId);

    /**
     * Deletes a correct answer
     * @param correctAnswer
     */
    void deleteCorrectAnswer(CorrectAnswer correctAnswer);

    /**
     * Deletes the given correct answers
     * @param correctAnswersList
     */
    void deleteCorrectAnswers(List<CorrectAnswer> correctAnswersList);

    /**
     * Saves a correct answer
     * @param correctAnswer
     */
    void saveCorrectAnswer(CorrectAnswer correctAnswer);

}
