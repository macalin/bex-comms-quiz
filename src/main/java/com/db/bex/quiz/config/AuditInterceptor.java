package com.db.bex.quiz.config;

import com.db.bex.quiz.model.AuditableEntity;
import com.db.bex.quiz.model.BaseEntity;
import org.hibernate.EmptyInterceptor;
import org.hibernate.Transaction;
import org.hibernate.type.Type;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by calimih on 06/07/2016.
 */
@Component
public class AuditInterceptor extends EmptyInterceptor {

    private static final Logger LOG = LoggerFactory.getLogger(AuditInterceptor.class);

    private int updates;
    private int creates;

    @Override
    public boolean onFlushDirty(Object entity, Serializable id, Object[] currentState, Object[] previousState, String[] propertyNames, Type[] types) {
        if (LOG.isDebugEnabled()) {
            updates++;
        }
        boolean currentStateModified = false;
        if (entity instanceof BaseEntity) {
            for ( int i=0; i < propertyNames.length; i++ ) {
                if ("dateCreated".equals( propertyNames[i]) && currentState[i] == null) {
                    currentState[i] = new Date();
                    currentStateModified = true;
                }
                if ("dateUpdated".equals( propertyNames[i])) {
                    currentState[i] = new Date();
                    currentStateModified = true;
                }
            }
        }
        return currentStateModified;
    }

    @Override
    public boolean onSave(Object entity, Serializable id, Object[] state, String[] propertyNames, Type[] types) {
        if (LOG.isDebugEnabled()) {
            creates++;
        }
        if ( entity instanceof BaseEntity) {
            for ( int i=0; i<propertyNames.length; i++ ) {
                if ("dateCreated".equals( propertyNames[i])) {
                    state[i] = new Date();
                    return true;
                }
            }
        }
        return false;
    }

    public void afterTransactionCompletion(Transaction tx) {
        if ( tx.wasCommitted() ) {
            LOG.debug("AuditInterceptor log: Creations: " + creates + ", Updates: " + updates);
        }
        updates=0;
        creates=0;
    }

}
