package com.db.bex.quiz.utils;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by calimih on 06/07/2016.
 */
public class HttpServerRequestUtils {

    public static String getApplicationUri(HttpServletRequest request, boolean isAdmin) {
        String uri = new StringBuilder(request.getScheme())
                .append("://")
                .append(request.getServerName())
                .append(":")
                .append(request.getServerPort())
                .append(request.getContextPath())
                .append("/index.html#!/login?"+ (isAdmin ? "isAdmin=true&" : "") )
                .toString();
        return  uri;
    }

}
