package com.db.bex.quiz.utils;

import org.apache.commons.lang3.text.WordUtils;
import org.springframework.stereotype.Component;

/**
 * Created by calimih on 04/07/2016.
 */
@Component
public class UserUtils {

    public static final String AT_DB_COM = "@db.com";

    public static final Character nameDelimiter = '-';

    public static String getEmail(String username) {
        return username.toLowerCase() + AT_DB_COM;
    }

    public static String getFirstName(String username) {
        int firstNameEndingIdx = username.indexOf(".");
        String firstName = username.substring(0, firstNameEndingIdx);
        return WordUtils.capitalize(firstName, nameDelimiter);
    }

    public static String getLastName(String username) {
        int lastNameStartingIdx = username.indexOf(".") + 1;
        String lastName = username.substring(lastNameStartingIdx, username.length());
        return  WordUtils.capitalize(lastName, nameDelimiter);
    }

}
