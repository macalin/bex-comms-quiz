package com.db.bex.quiz.utils;

import com.db.bex.quiz.model.UserRoleCode;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.impl.crypto.MacProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import javax.crypto.SecretKey;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by calimih on 01/07/2016.
 */
@Component
public final class TokenGenerator {

    @Autowired
    private Environment environment;

    private static SecretKey KEY = MacProvider.generateKey();
    private static int VALIDITY_HOURS_DEFAULT = 2;

    public String generateToken(String subject, boolean isAdmin){
        String jwtString = Jwts.builder().setSubject(subject)
                .setExpiration(getExpirationDate())
                .claim("role", isAdmin ? UserRoleCode.ADMIN.getCode() : UserRoleCode.USER.getCode())
                .signWith(SignatureAlgorithm.HS512, KEY)
                .compact();
        return jwtString;
    }

    private Date getExpirationDate(){
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.HOUR, environment.getProperty("token.validity.hours", Integer.class,VALIDITY_HOURS_DEFAULT));
        return cal.getTime();
    }

    public static Claims getTokenClaims(String token){
        Claims claims = Jwts.parser().setSigningKey(KEY)
               .parseClaimsJws(token).getBody();
        return claims;
    }

}
