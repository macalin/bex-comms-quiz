package com.db.bex.dao;

import com.db.bex.config.AppTestConfig;
import com.db.bex.quiz.dao.QuizDao;
import com.db.bex.quiz.model.Quiz;
import com.db.bex.quiz.model.QuizStatus;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

import java.util.List;

import static org.junit.Assert.assertNull;

/**
 * Created by calimih on 08/07/2016.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes= {AppTestConfig.class})
public class QuizDaoTest {

    @Autowired
    private QuizDao quizDao;

    @Autowired
    private PlatformTransactionManager ptm;

    @Test
    @Ignore
    public void testFindActiveQuizzes() {
        TransactionTemplate tx = new TransactionTemplate(ptm);

        tx.execute(new TransactionCallbackWithoutResult() {
            public void doInTransactionWithoutResult(TransactionStatus status) {
                List<Quiz> quizzes = quizDao.findQuizzesByStatus(QuizStatus.ACTIVE);

                assertNull("Quizzes list", quizzes);
//                assertEquals("Username", TOKEN_FIRST, userToken.getToken());
//                assertNotNull("Date Created", userToken.getDateCreated());
//                assertNull("Date Updated", userToken.getDateUpdated());
            }
        });
    }

}
