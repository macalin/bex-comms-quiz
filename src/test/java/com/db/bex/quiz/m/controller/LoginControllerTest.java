package com.db.bex.quiz.m.controller;

import com.db.bex.quiz.m.service.AuthenticationService;
import org.junit.Before;
import org.junit.Test;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.web.servlet.MockMvc;

import javax.servlet.http.HttpServletRequest;

import static org.easymock.EasyMock.*;
import static org.springframework.test.util.ReflectionTestUtils.setField;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

/**
 * Created by calimih on 04/07/2016.
 */
public class LoginControllerTest {

    private AuthenticationService authenticationService;

    private MockMvc mockMvc;

    @Before
    public void setup() throws Exception {
        LoginController loginController = new LoginController();
        authenticationService = createMock(AuthenticationService.class);
        setField(loginController, "authenticationService", authenticationService);
        this.mockMvc = standaloneSetup(loginController).build();
    }

    @Test
    public void testGenerateToken() throws Exception {
        authenticationService.generateToken(anyObject(HttpServletRequest.class), anyString());
        replay(authenticationService);

        mockMvc.perform(get("/login/user?username=mihail.calin").session(new MockHttpSession()))
                .andExpect(status().isOk());

        verify(authenticationService);
    }

}
