package com.db.bex.mail;

import com.db.bex.config.AppTestConfig;
import com.db.bex.quiz.mail.MailService;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Created by calimih on 30/06/2016.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes= {AppTestConfig.class})
@ActiveProfiles("dev")
public class MailServiceTest {

    @Autowired
    private MailService mailService;

    @Test
    @Ignore
    public void sendMail() {
        mailService.sendMail("mihail.calin@db.com", "great subject", "Aici ar trebui sa fie un link de activare");
    }

}
