package com.db.bex.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Created by calimih on 29/06/2016.
 */
public class ConnectionTest {

    static Connection con = null;

    public static void testMySQLConn(String schema, String user, String password) throws SQLException {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection("jdbc:mysql://fraitcf1vd3453.de.db.com:5306/" + schema, user, password);
        } catch (Exception ex) {
            System.out.println("Connection failed for schema: " + schema);
            ex.printStackTrace();
        } finally {
            con.close();
        }
    }

    public static void main (String args[]) throws SQLException {
        testMySQLConn("quiz_alex", "quiz_user", "quiz_user");
        testMySQLConn("quiz_andrei", "quiz_user", "quiz_user");
        testMySQLConn("quiz_oana", "quiz_user", "quiz_user");
        testMySQLConn("quiz_owner", "quiz_owner", "quiz_owner");
    }

}
