package com.db.bex.utils;

import com.db.bex.config.AppTestConfig;
import com.db.bex.quiz.utils.TokenGenerator;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Created by calimih on 04/07/2016.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes={AppTestConfig.class})
public class TokenGeneratorTest {

    @Autowired
    private TokenGenerator tokenGenerator;

    @Test
    public void testTokenGeneration() {
        String token = tokenGenerator.generateToken("alex.c", true);
        System.out.println(token);
    }

}
