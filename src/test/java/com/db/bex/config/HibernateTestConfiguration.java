package com.db.bex.config;

import com.db.bex.quiz.config.AuditInterceptor;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.hibernate4.HibernateTransactionManager;
import org.springframework.orm.hibernate4.LocalSessionFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.util.Properties;

/**
 * Created by calimih on 01/07/2016.
 */
@Configuration
@EnableTransactionManagement
@PropertySource(value = {"classpath:test-application.properties"})
public class HibernateTestConfiguration {

    @Autowired
    AuditInterceptor auditInterceptor;

    @Autowired
    private Environment environment;

    @Bean
    public LocalSessionFactoryBean sessionFactory() {
        LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean();
        sessionFactory.setDataSource(dataSource());
        sessionFactory.setPackagesToScan(new String[]{"com.db.bex.quiz.model"});
        sessionFactory.setHibernateProperties(hibernateProperties());
        sessionFactory.setEntityInterceptor(auditInterceptor);
        return sessionFactory;
    }

    @Bean
    public DataSource dataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(environment.getRequiredProperty("test.jdbc.driverClassName"));
        dataSource.setUrl(environment.getRequiredProperty("test.jdbc.url"));
        dataSource.setUsername(environment.getRequiredProperty("test.jdbc.username"));
        dataSource.setPassword(environment.getRequiredProperty("test.jdbc.password"));
        return dataSource;
    }

    private Properties hibernateProperties() {
        Properties properties = new Properties();
        properties.put("hibernate.dialect", environment.getRequiredProperty("test.hibernate.dialect"));
        properties.put("hibernate.show_sql", environment.getRequiredProperty("test.hibernate.show_sql"));
        properties.put("hibernate.format_sql", environment.getRequiredProperty("test.hibernate.format_sql"));
        return properties;
    }

    @Bean
    @Autowired
    public HibernateTransactionManager transactionManager(SessionFactory s) {
        HibernateTransactionManager txManager = new HibernateTransactionManager();
        txManager.setSessionFactory(s);
        return txManager;
    }

}
