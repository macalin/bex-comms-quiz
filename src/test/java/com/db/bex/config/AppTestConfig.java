package com.db.bex.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.*;
import org.springframework.core.env.Environment;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;

import java.util.Properties;

/**
 * Created by calimih on 01/07/2016.
 */
@Configuration
@ComponentScan(basePackages = { "com.db.bex.quiz" }, excludeFilters = { @ComponentScan.Filter(type = FilterType.ANNOTATION, value = Configuration.class) })
@Import({ HibernateTestConfiguration.class })
public class AppTestConfig {

    @Autowired
    private Environment environment;

    @Bean
    public JavaMailSenderImpl mailSender() {
        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
        mailSender.setHost(environment.getRequiredProperty("test.mail.host"));
        mailSender.setPort(Integer.parseInt(environment.getRequiredProperty("test.mail.port")));

        Properties javaMailProperties = new Properties();
        javaMailProperties.put("mail.debug", environment.getRequiredProperty("test.mail.debug"));
        mailSender.setJavaMailProperties(javaMailProperties);
        return mailSender;
    }

    @Bean
    public SimpleMailMessage templateMessage() {
        SimpleMailMessage templateMessage = new SimpleMailMessage();
        templateMessage.setFrom(environment.getRequiredProperty("test.mail.from"));
        templateMessage.setSubject(environment.getRequiredProperty("test.mail.subject"));
        return templateMessage;
    }

}
