"use strict";
var gulp = require('gulp');
var concat = require('gulp-concat');
var fs = require('fs');
var rev = require('gulp-rev');
var ngAnnotate = require('gulp-ng-annotate');
var uglify = require('gulp-uglify');
var del = require('del');
var inject = require("gulp-inject");
var replace = require("gulp-replace");
var rename = require("gulp-rename");
var templateCache = require("gulp-angular-templatecache");

gulp.task('clean',function() {
    return del([GLOBAL.paths.dest+'**/*'])
});

gulp.task('default', ['build']);

gulp.task("copy-css",['clean'], function () {
   return gulp.src(GLOBAL.normalizePaths(GLOBAL.resources.css))
        .pipe(gulp.dest(GLOBAL.paths.dest + "css"))
});

gulp.task("copy-fonts",['clean'], function () {
    return gulp.src(GLOBAL.normalizePaths(GLOBAL.resources.fonts))
        .pipe(gulp.dest(GLOBAL.paths.dest + "fonts"))
});

gulp.task("cache-templates", ['clean'], function() {
   return gulp.src(GLOBAL.normalizePaths(GLOBAL.resources.templates))
        .pipe(templateCache({standalone: true}))
        .pipe(gulp.dest(GLOBAL.paths.src));
});

gulp.task("concat-js",['clean', 'cache-templates'], function () {
    return gulp.src(GLOBAL.normalizePaths(GLOBAL.resources.javascript.external.concat(GLOBAL.resources.javascript.internal).concat(GLOBAL.resources.javascript.templates)), {base: GLOBAL.paths.src})
        .pipe(concat("app.js"))
        .pipe(rev())
        //.pipe(ngAnnotate())
        //.pipe(uglify())
        .pipe(gulp.dest(GLOBAL.paths.dest));
});

gulp.task("inject", ['clean', "copy-css", "concat-js"], function () {
    return gulp.src(GLOBAL.paths.src + "index_template.html")
        .pipe(rename("index.html"))
        .pipe(inject(gulp.src(GLOBAL.paths.dest + '*.js', {read: false}), {relative: true}))
        .pipe(inject(gulp.src(GLOBAL.paths.dest + "css/*.css", {read: false}), {relative: true}))
        .pipe(replace("../webapp/app/", "")) // replace wrong static paths
        .pipe(gulp.dest(GLOBAL.paths.dest));
});


gulp.task('build',['clean', 'cache-templates', 'copy-css', 'concat-js', 'copy-fonts', 'inject'],function() {

});

gulp.task('copy-html',['clean'],function() {
    return gulp.src(GLOBAL.normalizePaths('index.html'))
        .pipe(gulp.dest(GLOBAL.paths.dest))
});
