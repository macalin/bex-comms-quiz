"use strict";
var fs = require('fs');

GLOBAL.paths = {
    src: "src/main/ui/" ,
    dest: 'src/main/webapp/app/'
}

GLOBAL.resources;
GLOBAL.loadResources = function() {
    GLOBAL.resources = JSON.parse(fs.readFileSync("resources.json"));
}

GLOBAL.normalizePaths = function (values) {
    if (values instanceof Array) {
        return values.map(function (path) {
            return parsePath(path);

        });
    } else {
        return parsePath(values);
    }

    function parsePath(path) {
        if(path[0] == '!') {
            return '!'+GLOBAL.paths.src + path.substring(1);
        } else {
            return GLOBAL.paths.src + path;
        }
    }
}

GLOBAL.normalizeDestPaths = function (values) {
    if (values instanceof Array) {
        return values.map(function (path) {
            return parsePath(path);

        });
    } else {
        return parsePath(values);
    }

    function parsePath(path) {
        if(path[0] == '!') {
            return '!'+GLOBAL.paths.dest + path.substring(1);
        } else {
            return GLOBAL.paths.dest + path;
        }
    }
}


GLOBAL.loadResources();
require('require-dir')('./gulp-tasks');