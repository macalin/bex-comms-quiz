V1.1
- added maven version after application name
- fix single answer view: answer was not selected
- added disclaimer in login email
- active quiz: added 'Start Quiz'
- completed quiz: added 'View Results'
- quiz results: green text for correct answers
- change in info: 'missed correct answer', 'wrong user answer'
- Added Info next to 'i' icon, to be more visible. Added button class
- Text answer max length: 25 letters

V1.1.1
- active quiz: changed to 'Start'
- completed quiz: changed to 'Results'
- fix admin activate second quiz
- change 'Submit quiz' to 'Submit'
- set order for active/completed quizzes in quiz menu
- added Logout in active quiz
- token validity taken from properties file for email template
- technical: added maven build profiles for dev and prod
- after expired session, meaningful error => redirect to login page

V1.2.0
Added admin tab

V1.2.2
Added clause in email

V1.2.3
Fix security: user can see inactive quizzes
Fix security: user can view answers for an active quiz
Fix security: user can save an answer for an inactive quiz question
Add Quiz Q&A report
Change email password

V1.2.4
Fix PDF report bug
Prevent Double submit on login

V1.2.5
Added terms and conditions pop-up

V1.2.6
Auto-refresh user active quizzes list
Fix : remove error message on expired token on login

V1.2.7
Token validity 4 hours
Answer maxim length: 200 chars